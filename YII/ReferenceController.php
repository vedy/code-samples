<?php
/*
* Контроллер модуля «Расписание поликлиник»
* @vika.vedy
* 2010-2014
*/

class ReferenceController extends VVController
{

  public $enableAjaxValidation = true;
  public $enableClientValidation = true;
  public $errorMessageCssClass = 'error';
  public $attributes;
  private  $_refParent;
  private  $_refClinicParent;
  private  $_refListLink;
  private $_canmoderate;

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function init() {
    parent::init();

    $listpage =  Resource::model()->resetScope()->alias('t')->published()->findByAttributes(array('alias'=>'spravka','id'=>21));
    if(!$listpage) throw new CHttpException(404,'Страница не найдена');

    $this->_refParent = $listpage;


    $this->_refClinicParent =  Resource::model()->resetScope()->alias('t')->published()->withparent(21)->findByAttributes(array('alias'=>'raspisanie-poliklinik'));

    $this->_refListLink = '/spravka/raspisanie-poliklinik/';

    $this->_canmoderate = Yii::app()->user->hasAnyGroup(array('Администраторы','[Мед.редакторы]'));
    
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
    $this->pageTitle  = 'Справочная информация';

    $refpage =  $this->_refParent;

    $this->pageTitle = (!empty($refpage->longtitle))? $refpage->longtitle : $refpage->pagetitle;


    $this->render('/reference/index',array('content'=>$refpage->content));
	}


  /**
   *
   */
	public function actionListRegion() {

    $listpage =  $this->_refClinicParent;
    if(!$listpage) throw new CHttpException(404,'Страница не найдена');

    $this->publishAssets(
              false,
              array(),
              array(),
              array('jquery.ui')
    );

    $this->pageTitle = (!empty($listpage->longtitle))? $listpage->longtitle : $listpage->pagetitle;


    $regions = Resource::model()->resetScope()->alias('t')->with('children')->isfolder(1)->published()->withparent($listpage->id)->findAll();

    if($this->_canmoderate) {

      $this->publishAssets(
                false,
                array($this->assetsUrl.'/css/forms.css'=>'screen, projection'),
                array(),
                array('yiiactiveform','fancybox')
      );
    }

		$this->render('/reference/list',array('content'=>$listpage,'regions'=>$regions,'link'=>$this->_refListLink,'canmoderate'=>
    (Yii::app()->user->hasAnyGroup(array('Администраторы','[Мед.редакторы]')))));

	}


  /**
   *
   */
  public function actionShow($region,$organisation) {

    $listpage =  $this->_refClinicParent;
    if(!$listpage) throw new CHttpException(404,'Страница не найдена');
    
      $rpage = Resource::model()->resetScope()->alias('t')->published()->withparent($listpage->id)->findByAttributes(array('alias'=>$region));
      $ppage = ResourceClinic::model()->resetScope()->alias('t')->published()->withparent($rpage->id)->findByAttributes(array('alias'=>$organisation));
      if(!$rpage || !$ppage) throw new CHttpException(404,'Страница не найдена');
    $tvs = $ppage->getEavAttributesEx();
    
    if($this->_canmoderate) {

      if(Yii::app()->request->getParam('ajax') === 'edit' &&
         Yii::app()->request->getParam('attr',false) !== false ) {
        $this->showForm (Yii::app()->request->getParam('attr'),$ppage);
        Yii::app()->end();
      }
      else if (Yii::app()->request->getParam('ajax') === 'preview') {
        if(($newtvs = Yii::app()->request->getParam(get_class($ppage).'_tvs',false)) !== false)
          $ppage->setEavAttributes($newtvs);
        $this->saveModelBlock (Yii::app()->request->getParam(get_class($ppage)),$newtvs,$ppage);
        Yii::app()->end();
      }


      $this->cs->registerCssFile($this->assetsUrl.'/css/forms.css', 'screen');
      $this->cs->registerScriptFile($this->assetsUrl.'/js/fancybox/jquery.mousewheel-3.0.4.pack.js',CClientScript::POS_HEAD);
      $this->cs->registerScriptFile($this->assetsUrl.'/js/fancybox/jquery.fancybox-1.3.4.pack.js',CClientScript::POS_HEAD);
      $this->cs->registerCssFile($this->assetsUrl.'/js/fancybox/jquery.fancybox-1.3.4.css', 'screen');
      $this->cs->registerCoreScript('yiiactiveform');
      
    }
    

    $this->pageTitle  = $ppage->pagetitle.' | '.$listpage->longtitle;
    $template = ($this->_canmoderate)?'/reference/clinic_moderate':'/reference/clinic';
		$this->render(
      $template,
      array('model' => $ppage,
           'content'=>$ppage,
           'region'=>$rpage,
           'backlink'=>$this->_refListLink,
           'link'=>$this->_refListLink.$rpage->alias.'/'.$ppage->alias,
           'tvs'=>$tvs,
           'preview'=>false,
           'assetsUrl'=>$this->assetsUrl));
    Yii::app()->end();

  }


  /**
   * показываем ajax-форму редактирования для одельных полей
   * @param string $attr аттрибут, который редактируем
   * @param CModel $model модель
   */ 
  protected function showForm ($attr,$model) {
    $tvs = $model->getEavAttributesEx();
?>
      
<div class="resource-form-area">
  <?php
      $form = $this->beginWidget('CActiveForm', array(
        'id' => 'preview',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>false,
        'focus'=>array($model,'pagetitle'),
        'clientOptions'=>array('validateOnSubmit'=>true,'validateOnChange'=>false),
        'htmlOptions'=>array('style'=>'width: '.Yii::app()->request->getPost('w',500).'px')
    ));
    echo CHtml::hiddenField('ajax','preview');
    
    if($attr == 'content_header') { ?>

  <div class="row">
    <?php echo $form->labelEx($model,'pagetitle'); ?>
    <?php echo $form->textField($model,'pagetitle'); ?>
    <?php echo $form->error($model,'pagetitle'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model,'longtitle'); ?>
    <?php echo $form->textField($model,'longtitle'); ?>
    <?php echo $form->error($model,'longtitle'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model,'content'); ?>
    <?php echo $form->textArea($model,'content',array('class'=>'editor-textarea','rows'=>10)); ?>

  </div>
  <?php

    }

    
    else { ?>
  <div class="row <?php echo $attr; ?>">
    <?php echo CHtml::label($tvs[$attr]['caption'],get_class($model).'['.$attr.']'); ?>
    <?php echo CHtml::textArea(get_class($model).'_tvs['.$attr.']',$tvs[$attr]['value'],array('class'=>'editor-textarea','rows'=>10));?>

  </div>
  <?php

    }
    if($attr == 'content_header') {
      echo $form->errorSummary($model);
      echo CHtml::submitButton(
              'Сохранить! '.date('H:i:s'),
              array('name'=>get_class($model).'[do][saveheader]','id'=>get_class($model).'_do_saveheader'));

    }
    else echo CHtml::ajaxSubmitButton(
      'Сохранить '.date('H:i:s'),null,array(),
      array('name'=>get_class($model).'[do][preview]','id'=>get_class($model).'_do_preview'));
  
    echo CHtml::button('Отменить и закрыть',array('name'=>get_class($model).'[do][cancel]','id'=>get_class($model).'_do_cancel','onclick'=>'$.fancybox.close();'));
      $this->endWidget(); ?>
</div>
  <?php
  }


  /**
   * Сохранение отдельных блоков при редактировании ajax-ом
   * @throws CHttpException
   * @param $params чего сохраняем?
   * @param $newtvs допполя, которые сохраняем
   * @param $model
   * @return void
   */
  protected function saveModelBlock ($params,$newtvs,$model) {
    if(!Yii::app()->user->hasAnyGroup(array('Администраторы','[Мед.редакторы]')))
      throw new CHttpException(404,'Страница не найдена');


    $saveErrors = CActiveForm::validate($model);
    if ($saveErrors == '[]') {
      $jevix = $this->configTypo();

      if($newtvs && count($newtvs)) {
        list($attr)    = array_keys($newtvs);
        $newtvs[$attr] = $jevix->parse($newtvs[$attr], $errors);
        $model->setEavAttributes($newtvs);
      }
      else {
        $model->content   = $jevix->parse($model->content, $errors);
        $model->pagetitle = preg_replace('/[^-а-яА-Я №(),0-9]/iu','',$model->pagetitle);
      }

      $model->save();
      $tvs = $model->getEavAttributesEx();
      if (isset($params['pagetitle'])) {
$html =<<<END
<div class="sideheader med">
    <h2>{$model->pagetitle}
      <div class="smalltitle">{$model->longtitle}</div>
    </h2>
  </div>

  <div class="med" id="info">
    {$model->content}
  </div>
END;
      }
      else {
        $content = empty($tvs[$attr]['value'])?'Нет информации':$tvs[$attr]['value'];
$html =<<<END
      <h3>{$tvs[$attr]['caption']}:</h3>
      {$content}
END;
      }

      echo json_encode(array('html'=>$html));
    }
    else echo var_export(json_decode($saveErrors,1),1);

  }

  /**
   * Показываем форму добавления организации
   * @throws CHttpException
   * @param $region
   * @return void
   */
  public function actionAddnew($region) {
    if(!Yii::app()->user->hasAnyGroup(array('Администраторы','[Мед.редакторы]')))
      throw new CHttpException(404,'Страница не найдена');

    $listpage =  $this->_refClinicParent;
    if(!$listpage) throw new CHttpException(404,'Страница не найдена');
      $rpage = ResourceClinic::model()->resetScope()->alias('t')->published()->withparent($listpage->id)->findByAttributes(array('alias'=>$region));
    if(!$rpage) throw new CHttpException(404,'Страница не найдена');

    $parent = $rpage->id;
    $nextmenuindex = 1;
    $builder  = Yii::app()->db->commandBuilder;
    $criteria = new CDbCriteria(array('select' => "MAX(menuindex)"));
    $criteria->addCondition("parent = {$listpage->id}");
    $maxmenuindex  = $builder->createFindCommand( ResourceClinic::model()->tableName(),	$criteria	)->queryScalar();
    if($maxmenuindex) $nextmenuindex = $maxmenuindex+1;


    $model = new ResourceClinic();
    $model->type='document';
    $model->contentType  = 'text/html';
    $model->published    = 1;
    $model->pub_date     = 0;
    $model->unpub_date   = 0;
    $model->parent       = $parent;
    $model->isfolder     = 0;
    $model->richtext     = 1;
    $model->template     = $rpage->template;
    $model->menuindex    = $nextmenuindex;
    $model->searchable   = 1;
    $model->cacheable    = 1;
    $model->deleted      = 0;
    $model->donthit      = 0;
    $model->privateweb   = 0;
    $model->privatemgr   = 0;
    $model->content_dispo =0;
    $model->hidemenu     = 1;
    $model->uri          ='spravka/'.$this->_refClinicParent->alias.'/'.$rpage->alias.'/';
    $model->class_key    ='modDocument';
    $model->context_key  ='web';
    $model->content_type = 1;


    $tvs = $model->getEavAttributesEx();

    $ajaxaction = 'new-resource';
    $this->performAjaxValidationAndSave($model,$ajaxaction,$rpage);

    $this->publishAssets(
              false,
              array($this->assetsUrl.'/css/forms.css'=>'screen, projection'),
              array(),
              array('jquery.ui','fancybox')
    );

    $this->pageTitle  = 'Новое учреждение | '.$listpage->longtitle;
    $this->render(
                  '/reference/addclinic',
                  array('model'=>$model,
                        'ajaxaction'=>$ajaxaction,
                        'model_class'=>get_class($model),
                        'backlink'=>$this->_refListLink,
                        'tvs'=>$tvs,
                        'assetsUrl'=>$this->assetsUrl));

    Yii::app()->end();
  }

  /**
   * Сохранение/предпросмотр новой модели
   * @param $model собсно модель
   * @param $ajaxaction проверочный параметр
   * @param $rpage родительский раздел
   * @return void
   */
  protected function performAjaxValidationAndSave($model,$ajaxaction,$rpage)
  {
    /*
     * TODO: 1.: При сохранении блоков текста: добавить парсинг на предмет картинок и их ширины-высоты + ресайз картинок по полученным данным (с сохранением оригинала) и добавлением ссылки на оригинал, если размер был изменён
     */
    if(!Yii::app()->user->hasAnyGroup(array('Администраторы','[Мед.редакторы]')))
      throw new CHttpException(404,'Страница не найдена');
    
    if(Yii::app()->request->getParam('ajax') === $ajaxaction) {
          $model->attributes=$_POST[get_class($model)];
          $model->pagetitle = preg_replace('/[^-а-яА-Я №(),0-9]/iu','',$model->pagetitle);
          $saveErrors = CActiveForm::validate($model);

          $jevix = $this->configTypo();
          $model->content = $jevix->parse($model->content, $errors);
        
          $newtvs = Yii::app()->request->getParam(get_class($model).'_tvs',array());
          foreach ($newtvs as $attr=>$content) $newtvs[$attr] = $jevix->parse($content, $errors);

          if(count($newtvs)) {$model->setEavAttributes($newtvs); $tvs = $model->getEavAttributesEx();}

          if ($saveErrors == '[]') {
            if(Yii::app()->request->getParam(get_class($model).'_action') === 'preview') {
              echo json_encode(array(
                                      'html'=>  $this->renderPartial(
                                                                '/reference/clinic',
                                                                array('content'=>$model,
                                                                      'region'=>$rpage,
                                                                      'backlink'=>$this->_refListLink,
                                                                      'link'=>$this->_refListLink.$rpage->alias.'/'.$model->alias,
                                                                      'tvs'=>$tvs,
                                                                      'preview'=>true
                                                                ),
                                                               1)
                               )
              );
            }
            else {
              $model->attributes=$_POST[get_class($model)];
              if(count($newtvs)) $model->setEavAttributes($newtvs);
              if($model->save())
                echo json_encode(array('url'=>$this->_refListLink.$rpage->alias.'/'.$model->alias.'.html'),1);
              else echo $model->hasErrors();
            }
          }
          else echo $saveErrors;
          Yii::app()->end();
    }
  }

  /**
   * Создание и настройка типографа
   * @return Jevix
   */
  private function configTypo () {
    require_once('jevix-1.1/jevix.class.php');
    $jevix = new Jevix();
    $jevix->cfgAllowTags(array('i','em', 'b', 'u', 'strong', 'nobr', 'li', 'ol', 'ul', 'sup', 'br', 'p', 'table','tr','th','td','thead','tbody','img'));
    $jevix->cfgSetTagChilds('td',array('i','em', 'b', 'u', 'strong', 'nobr', 'sup', 'br'));
    $jevix->cfgSetTagChilds('th',array('i','em', 'b', 'u', 'strong', 'nobr', 'sup', 'br'));
    $jevix->cfgAllowTagParams('p', array('style'));
    $jevix->cfgAllowTagParams('td', array('colspan','rowspan','style','width'));
    $jevix->cfgAllowTagParams('th', array('colspan','rowspan','style','width'));
    $jevix->cfgAllowTagParams('tr', array('colspan','rowspan'));
    $jevix->cfgAllowTagParams('img', array('src'=>'#imagelocal', 'style', 'alt' => '#text', 'title', 'align' => array('right', 'left', 'center'), 'width' => '#int', 'height' => '#int', 'hspace' => '#int', 'vspace' => '#int'));

    $jevix->cfgSetTagShort(array('br','img'));
    $jevix->cfgSetTagCutWithContent(array('script', 'object', 'iframe', 'style'));
    $jevix->cfgSetAutoReplace(array('+/-', '(c)', '(r)'), array('±', '©', '®'));

    $jevix->cfgSetTagParamsRequired('img', 'src');

    $jevix->cfgSetAutoBrMode(false);

    return $jevix;
  }

  public function actionDelete($id) {
    if(!Yii::app()->user->hasAnyGroup(array('Администраторы','[Мед.редакторы]')))
      throw new CHttpException(404,'Страница не найдена');

    $ppage = ResourceClinic::model()->resetScope()->alias('t')->published()->findByAttributes(array('id'=>$id));
    if(!$ppage) throw new CHttpException(404,'Страница не найдена');
    $ppage->published = 0;
    $ppage->deleted = 1;
    $ppage->save();
    $this->redirect($this->_refListLink);

  }

  public function actionAddregion() {
    
    $nextmenuindex = 1;
    $builder  = Yii::app()->db->commandBuilder;
    $criteria = new CDbCriteria(array('select' => "MAX(menuindex), MAX(id)"));
    $criteria->addCondition("parent = {$this->_refClinicParent->id}");
    $criteria->addCondition("published = 1");
    $criteria->addCondition("isfolder = 1");
    $need_params  = $builder->createFindCommand( ResourceRegion::model()->tableName(),	$criteria	)->queryRow();

    list($maxmenuindex,$lastregionid) = array_values($need_params);
    if($maxmenuindex) $nextmenuindex = $maxmenuindex+1;
    
    $regionlast = ResourceRegion::model()->resetScope()->alias('t')->isfolder(1)->published()->findByAttributes(array('id'=>$lastregionid));
    $regionlast = $regionlast->attributes;
    
    unset($regionlast['id'],$regionlast['pagetitle'],$regionlast['alias'],$regionlast['createdon'],$regionlast['editedon'],$regionlast['publishedon'],$regionlast['publishedby']);
    
    $regionlast['uri'] = 'spravka/'.$this->_refClinicParent->alias.'/';
    $regionlast['menuindex'] = $nextmenuindex;

    $model = new ResourceRegion();
    foreach ($regionlast as $attr=>$value)
      $model->$attr = $value;

    if(Yii::app()->request->getPost('ResourceRegion',false)) {
      $saveErrors = CActiveForm::validate($model);
      //$model->attributes=$_POST['ResourceRegion'];
      if($saveErrors == '[]' && $model->save()) {
        $html = <<<END
        <h3> <a href="#{$model->alias}">{$model->pagetitle}</a> </h3>
        <div class="accordion-content"><a name="{$model->alias}"></a>
          <br />
          <p class='itemtags'><a href="{$this->_refListLink}{$model->alias}/add-new-clinic.html">Добавить учреждение/поликлинику в этом районе</a></p>
        </div>
END;
        exit (json_encode(array('html'=>$html)));
      }
      else if($saveErrors == '[]') echo $model->hasErrors();
      else echo $saveErrors;
    }
    else {
      Yii::app()->clientScript->corePackages = array();
      $this->renderPartial('_createRegion',array('model'=>$model,'w'=>Yii::app()->request->getPost('w',500)),false,true);
    }

  }

}
