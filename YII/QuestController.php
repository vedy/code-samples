<?php
/*
* Контроллер модуля «Городской квест»
* @vika.vedy
* 2010-2014
*/

  class QuestController extends VVController {
    public $model;
    public $quests;
    public $teams_list;
    public $quest_log;
    public $uri;
    public $defaultAction = 'index';


    public function init() {
      parent::init();
      mb_internal_encoding('UTF-8');
      setlocale(LC_ALL, "ru_RU.UTF8");
      $this->uri = 'quest/';

      $this->loadModel($this->uri);
      $this->teams_list = Yii::getPathOfAlias('application.runtime') . '/teams-' . $this->model->id . '.log';
      $this->quest_log  = Yii::getPathOfAlias('application.runtime') . '/quest-' . $this->model->id . '.log';
      $this->publishAssets(false, array($this->assetsUrl . '/css/gh-buttons.css' => 'screen, projection'));
      $this->loadQuests();
    }

    /**
     * Главная страница раздела квестов
     * Внизу список последних 10 квестов с кнопками редактирования для модератора
     */
    public function actionIndex() {
      $this->pageTitle = 'Раздел квестов';
      //exit('zzz'.date('Y-m-d h:i:s'));
      $this->pageTitle = $this->model->pagetitle;

      $last_quest_model = $this->quests[0];
      $least_time       = $last_quest_model->pub_date - time();
      if($least_time > 0) { //ещё есть время до начала квеста
        $this->model->content .= $this->generateCountdown('До начала квеста осталось: ',$least_time,$last_quest_model->pub_date);
        $this->publishAssets(false, array($this->assetsUrl . '/css/forms.css' => 'screen, projection'), array(), array('counter'));
      }
      else  if ($last_quest_model->searchable || $least_time < -3600*24 ) {
        $this->model->content .= $this->generateEndStory($last_quest_model,true);
      }
      $this->render('/quest/index', array('model' => $this->model, 'quests' => $this->quests, 'moderate' => $this->checkAccess(false)));
    }


    /* Страница квеста или задания с запароленным блоком. Для страницы квеста у модераторов показывается контрольная панель статистики? */
    public function actionCmspage($alias,$teamid=null) {

      $uris = array($this->uri . $alias);
      if (substr($alias, -5) != '.html') array_push($uris, $this->uri . $alias . '.html', $this->uri . $alias . '/');

      $dependency         = new CDbCacheDependency();
      $dependency->sql    = 'SELECT MAX(IF( editedon >0, editedon, createdon )) FROM modxrev_site_content WHERE uri' . ((count($uris) > 1) ? ' IN (:aliases)' : '=:aliases');
      $dependency->params = array(':aliases' => ((count($uris) > 1) ? "'" . implode("','", $uris) : $alias));
      //exit(print_r($uris,1));
      $model = Resource::model()->resetScope()->alias('res');
      if (!Yii::app()->user->isAdmin())
        $model->getDbCriteria()->addSearchCondition('published', 1, false, 'AND', '=');
      $model->getDbCriteria()->addInCondition('uri', $uris);
      $model->cache(36, $dependency);
      $model = $model->find();
      if (!$model) throw new CHttpException(404, 'Указанная запись не найдена');


      $this->quest_log = Yii::getPathOfAlias('application.runtime') . '/quest-' . $model->id . '.log';
      $quest           = $this->loadQuest($model->id);

      $teams = array();
      $this->teams_list = Yii::getPathOfAlias('application.runtime') . '/teams-' . $model->id . '.log';
      if(file_exists($this->teams_list)) {
        $tlist = parse_ini_file($this->teams_list,true);
        foreach ($tlist as $team_name => $team_props) {
          $teams[$team_name] = $team_props;
        }
      }

      if ($teamid && Yii::app()->request->getParam('tipshown') && Yii::app()->request->getParam('tippart')) {
        $uri   = '/'.str_replace('.html','/',$model->uri).$teamid.'.html';
        file_put_contents($this->quest_log,
                    '[' . date('Y-m-d H:i:s') . ']  |*| '. time().' |*| '. Yii::app()->user->forumId.' |*| '.Yii::app()->user->fullname.' |*| '. $teamid .' |*| '. Yii::app()->request->getParam('tippart') .' |*| tip shown '. Yii::app()->request->getParam('tipshown') . "\r\n", FILE_APPEND);
        $this->redirect($uri);
      }
      $chunks         = preg_split('/\[\/?(password-protection=*[-а-яё\d\w\/: +=&;*,.#]+)?\]/uis', $model->content, -1, PREG_SPLIT_DELIM_CAPTURE);
      $model->content = array_shift($chunks);
      $least_time     = $model->pub_date - time();
      if($least_time > 0) { //ещё есть время до начала квеста
        $model->content .= $this->generateCountdown('До начала квеста осталось: ',$least_time,$model->pub_date);
      }

      else if (!$teamid || $model->searchable || $least_time < -3600*24 ) {
        //если посетитель не по командной ссылке или игра окончена или с момента начала квеста прошло более суток - покажем сообщение о том, что игра началась/закончилась и список команд
        if ($model->searchable || $least_time < -3600*24 ) {
          $model->searchable = true;
          $model->content .= $this->generateEndStory($model);
        }
        else $model->content .= $this->generateStartStory($model,$teams);
      }
      else if ($teamid && $least_time<=0) {
        //если это страница команды и игра началась, уберём описание квеста, чтобы команде было видно только прохождение игры
        $model->content = '';
      }

      if ($least_time<=0 && ($teamid || $model->searchable)) {
        $model->content .= '<h1>Добро пожаловать в игру!</h1>';
        $p = 0; $prev_password_given_time = 0;
        foreach ($chunks as $part_id => $chunk) {
          if (stripos($chunk, 'password-protection=') === 0 && isset($chunks[$part_id + 1])) {
            $match   = preg_match('/password-protection=([^\/]+)(\/\/([0-9:]+))?(\/\/check)?/i',$chunk,$matches);

            //CVarDumper::dump($matches,10,1); exit();
            if($match) {
              $password = $matches[1];
              $time     = (isset($matches[2],$matches[3]))? $matches[3] : '12:00'; list($starthours,$startmins) = explode(':',$time);
              $check    = isset($matches[4]);
              $part_show_time = ($check || $part_id==0)?strtotime("0:00",$model->pub_date) + 3600*$starthours+60*$startmins:(!empty($prev_password_given_time)?$prev_password_given_time:0);

              $p++;
              $passwId1 = sha1($model->id . 'passw' . $p);
              $passwId2 = sha1($model->id . 'passw' . $part_id);
              $chunktext= strip_tags(str_ireplace('[/password-protection]', '', $chunks[$part_id + 1]));
              if ($model->searchable) {
                //квест уже оконечен и открыт для всех, публикуем задания, подсказки, коды
                $model->content .= $this->generatePartChunk($p,$chunktext, $password,0,0);
              }
              else {
                $passwClean = normalize_str($password);
                $password_given = false;
                //CVarDumper::dump(array($passwClean,$p),10,1);
                if(file_exists($this->quest_log)) {
                  $attempts = file($this->quest_log, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                  foreach ($attempts as $string) {
                    list($timestamp,$ttt,$mid,$mlogin,$team,$part,$givenpass) = explode(' |*| ',$string);
                    $time = intval($ttt); $part = intval($part);
                    if ($part==$p && $team == $teamid && normalize_str($givenpass) == $passwClean) {
                      //пароль к этой части открыт! показываем эту часть и, если подошло время - открываем следующую часть, если нет - показываем таймер к след. части
                      $model->content .= $this->generatePartChunk($p,$chunktext, $password,$time,$part_show_time,$check);
                      $password_given = true;
                      $prev_password_given_time = $time;
                      //CVarDumper::dump(array('$part_id'=>$p,'$chunks'=>sizeof($chunks)/2),10,1);
                      if ($p == sizeof($chunks)/2)  {
                        //это последняя часть и она открыта! поздравляем, выводим время прохождения
                        $date1 = new DateTime(); $date1->setTimestamp($model->pub_date);
                        $date2 = new DateTime(); $date2->setTimestamp($prev_password_given_time);
                        $interval = $date1->diff($date2);
                        $diff =  $interval->h . " ч. " . $interval->i." м. ".$interval->s." с. ";

                        $model->content .= $this->renderPartial('/quest/finished',
                          array('time' => $diff.' <span class="tip">(с '.strftime('%H:%M:%S',$model->pub_date).' по '.strftime('%H:%M:%S',$prev_password_given_time).')</span>',
                                'model'=>$model), 1);
                      }
                      break;
                    }
                  }
                }
                if (!$password_given) {
                  $model->content .= $this->generatePartChunk($p,$chunktext,false,0, $part_show_time,$check);
                  if($password) $model->content .= $this->generatePasswordForm($p, $model,$teamid);
                  break;
                }
              }
            }
          }
        }
      }

      $log     = (file_exists($this->quest_log)) ? file($this->quest_log, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) : array();

      $this->publishAssets(false, array($this->assetsUrl . '/css/forms.css' => 'screen, projection'), array(), array('counter'));
      $this->pageTitle = $model->pagetitle;
      $this->render('/quest/page',
        array('model' => $model,
              'moderator' => $this->checkAccess(false),
              'log' => $teamid?array():$log,
              'qpasswords' => $teamid?array():$quest['passwords'],
              'teams'=>$teamid?array():$teams));
    }


    private function generateCountdown($text,$time,$starttime,$images='/img/digits.png',$template='countdown',$data = array()) {
      $time_days  = floor($time/(3600*24)); $least_sec = $time-$time_days*3600*24;
      $time_hours = floor($least_sec/3600); $least_sec = $least_sec-$time_hours*3600;
      $time_mins  = floor($least_sec/60);   $time_secs = $least_sec-$time_mins*60;
      $mask = 'dd,hh:mm:ss';
      if (!$time_days) {
        $mask = ($time_hours)?'hh:mm:ss':'mm:ss';
        $time = ($time_hours)?implode(':',array(
                                  str_pad($time_hours,2,'0',STR_PAD_LEFT),
                                  str_pad($time_mins,2,'0',STR_PAD_LEFT),
                                  str_pad($time_secs,2,'0',STR_PAD_LEFT))) :
                              implode(':',array(
                                  str_pad($time_mins,2,'0',STR_PAD_LEFT),
                                  str_pad($time_secs,2,'0',STR_PAD_LEFT)));
      }
      else $time = str_pad($time_days,2,'0',STR_PAD_LEFT).','.implode(':',array(
                                     str_pad($time_hours,2,'0',STR_PAD_LEFT),
                                     str_pad($time_mins,2,'0',STR_PAD_LEFT),
                                     str_pad($time_secs,2,'0',STR_PAD_LEFT)));

      //exit(strftime('%e %B, %Y %H:%M',$starttime).print_r(array($time_hours,$time_mins,$time_secs,$time,$mask),1));
      return $this->renderPartial('/quest/'.$template, array('text'=>$text, 'time' => $time, 'image'=>$images, 'format'=>$mask, 'data'=>$data), 1);
    }

    private function generateEndStory($model,$index=false) {
      $uri = $index?'http://'.$_SERVER['HTTP_HOST'].'/'.$model->uri:false;
      return $this->renderPartial('/quest/finally', array('questid' => $model->id, 'url'=>$uri), 1);
    }

    private function generateStartStory($model,$teams) {
      return $this->renderPartial('/quest/start', array('questid' => $model->id, 'teams'=>$teams), 1);

    }

    private function generatePasswordForm($part, $model, $teamcode) {
      $tips = array('Отличная попытка! Но попробуйте ещё раз:',
                    'Молоток! Но не получилось :( <img src="/forum/Smileys/smiles/bu.gif" /> Попробуйте ещё раз:',
                    'Не принято! <img src="/forum/Smileys/smiles/bu.gif" /> Подумайте ещё немного:',
                    'Попытка — не пытка %-) <img src="/forum/Smileys/smiles/bu.gif" /> Давайте ещё раз попробуем:',
                    'Спасибо за старание! <img src="/forum/Smileys/smiles/bu.gif" /> На этот раз не вышло. Подумайте ещё:',
                    'Вода камень точит. <img src="/forum/Smileys/smiles/bu.gif" /> И у вас всё получится! Ещё разок?',
                    'Неудачная попытка, но <img src="/forum/Smileys/smiles/bu.gif" /> можно попробовать ещё раз:');
      $replytext = '';
      if (Yii::app()->request->getParam('given') && $part==Yii::app()->request->getParam('givenpart')) {
        $replytext = '<p style="line-height: 40px; text-align: center; color: darkred;"> Код «'.rawurldecode(Yii::app()->request->getQuery('given')).'» не подходит.<br />'.$tips[rand(0,sizeof($tips)-1)].'</p>';
      }
      return $this->renderPartial('/quest/passwordform', array('part' => $part, 'questid' => $model->id, 'teamcode'=>$teamcode, 'replytext'=>$replytext), 1);
    }

    private function generatePartChunk($p, $text, $password, $passwordgiventime=0, $starttime,$checktime=false) {
      //exit(strftime('%H:%M:%S',$starttime).' / '.strftime('%H:%M:%S'));
      if($starttime <= time() || !$checktime ) {
        $tips     = array();
        if (preg_match_all('/\[\/?part-tip=?([^\/]+)(\/\/([0-9]+))?\]/uis', $text, $tips_match)) {
          $text = nl2br(preg_replace('/\[\/?(part-tip=?.*?)\]/uis','',$text));

          foreach($tips_match[1] as $tip_id=>$tip_text) {
            $tip_time = (isset($tips_match[3][$tip_id])?$tips_match[3][$tip_id]:15)*60+$starttime;
            if($tip_time <= time() || !empty($password)) $tips[]   = '<b>'.$tip_text.'</b> <span class="tip">('.strftime('%H:%M:%S',$tip_time).')</span>';
            else { $tips[] = $this->generateCountdown('',$tip_time-time(),$tip_time,'/img/small-digits.png','tipcnt',array('tipid'=>($tip_id+1),'partid'=>$p)); break; }
          }
        }
        $text = preg_replace('/\[\/?(part-tip=?.*?)\]/uis','',$text);
        return $this->renderPartial('/quest/part',
          array('p' => $p.($starttime?' <span class="tip">('.strftime('%H:%M:%S',$starttime).')</span>':''),
                'text'=>$text,
                'passw' => $password,
                'passwtime' => $passwordgiventime?strftime('%H:%M:%S',$passwordgiventime) : '',
                'tips'=>$tips), 1);
      }

    }

    public function actionUnlock($id) {
      $teamcode = Yii::app()->request->getPost('teamcode');
      if (!empty($teamcode)) {
        $quest = $this->loadQuest($id);
        $p     = Yii::app()->request->getPost('quest');
        if (isset($p[$id]) && !empty($p[$id]['password']) && $quest !== false) {
          $uri   = '/'.str_replace('.html','/',$quest['uri']).$teamcode.'.html';

          $givenPart = $p[$id]['part'];
          $givenPassw = normalize_str($p[$id]['password']);

          file_put_contents(Yii::getPathOfAlias('application.runtime') . '/quest-' . $id . '.log',
            '[' . date('Y-m-d H:i:s') . ']  |*| '. time().' |*| '. Yii::app()->user->forumId.' |*| '.Yii::app()->user->fullname.' |*| '. $teamcode .' |*| '. $givenPart .' |*| '.$p[$id]['password'] . "\r\n", FILE_APPEND);
          $this->redirect($uri . '?givenpart='.$givenPart.'&given='.rawurlencode($p[$id]['password']));
        }
        $this->redirect($_SERVER['REQUEST_URI'] . '?не-считается');
      }
      else throw new CHttpException(503, 'Вы должны быть авторизованы для участия в квесте');
      Yii::app()->end();
    }


    /* редактирование текста страницы квестов */
    public function actionModerate() {
      $this->checkAccess();

      if (count(Yii::app()->request->getPost(get_class($this->model)))) {
        $saveErrors = CActiveForm::validate($this->model);
        if ($saveErrors == '[]' && $this->model->save()) {
            $this->redirect('/' . $this->model->uri);
        }
      }
      $this->pageTitle = 'Редактор квестов';
      $this->publishAssets(false, array($this->assetsUrl . '/css/forms.css' => 'screen, projection'), array(), array('jquery.ui', 'yiiactiveform'));
      $this->render('/quest/moderate/update', array('model' => $this->model, 'quests' => $this->quests));

    }


    /**
     * Добавляем квест: текстовый редактор с параметрами пароля. Ресурс добавляется как папка
     */
    public function actionAdd() {
      exit('addquest');
      $this->checkAccess();

      $newmodel = new ResourceQuest();
      $this->actionEdit($newmodel);

    }

    /**
     * Панель управления квестом: блок статистики, текстовый редактор с параметрами пароля + список приаттаченых квест-страниц и возможностью добавлять страницы
     */
    public function actionCopy($id = null) {
      $this->checkAccess();
      if (!$id)
        throw new CHttpException(404, 'Указанная запись не найдена');

      $model = ResourceQuest::model()->resetScope()->alias('res');
      $model->getDbCriteria()->addSearchCondition('parent', $this->model->id, false, 'AND', '=');
      $model->getDbCriteria()->addSearchCondition('id', $id, false, 'AND', '=');
      $model = $model->find();
      if (!$model)
        throw new CHttpException(404, 'Указанная запись не найдена');

      $newmodel             = new ResourceQuest();
      $newmodel->attributes = $model->attributes;
      $newmodel->id         = null;
      $newmodel->searchable = 0;
      $newmodel->pagetitle  = '';
      $newmodel->setIsNewRecord(true);
      $this->actionEdit(null, $newmodel);
    }

    /** Удаление квеста **/
    public function actionDelete($id = null) {
      $this->checkAccess();
      if (!$id)
        throw new CHttpException(404, 'Указанная запись не найдена');

      $model = ResourceQuest::model()->resetScope()->alias('res');
      $model->getDbCriteria()->addSearchCondition('parent', $this->model->id, false, 'AND', '=');
      $model->getDbCriteria()->addSearchCondition('id', $id, false, 'AND', '=');
      $model = $model->find();
      if (!$model)
        throw new CHttpException(404, 'Указанная запись не найдена');

      if($model->delete()) $this->redirect('/quest/moderate');
      throw new CHttpException(404, 'Указанная запись не найдена');
    }

    /**
     * Панель управления квестом: блок статистики, текстовый редактор с параметрами пароля + список приаттаченых квест-страниц и возможностью добавлять страницы
     */
    public function actionEdit($id = null, &$newmodel = null) {
      $this->checkAccess();
      setlocale(LC_TIME, "ru_RU.UTF8");
      if (!$id && $newmodel) {
        $model = $newmodel;
      }
      else {
        $model = ResourceQuest::model()->resetScope()->alias('res');
        $model->getDbCriteria()->addSearchCondition('parent', $this->model->id, false, 'AND', '=');
        $model->getDbCriteria()->addSearchCondition('id', $id, false, 'AND', '=');
        $model = $model->find();
        if (!$model)
          throw new CHttpException(404, 'Указанная запись не найдена');
      }
      $errors = '';
      if (count(Yii::app()->request->getPost(get_class($model)))) {
        if (!Yii::app()->request->getQuery('id')) {
          //exit('Новый квест!');
          $nextmenuindex = 1;
          $builder       = Yii::app()->db->commandBuilder;
          $criteria      = new CDbCriteria(array('select' => "MAX(menuindex)+1"));
          $criteria->addCondition("parent = {$this->model->id}");
          $maxmenuindex = $builder->createFindCommand(Resource::model()->tableName(), $criteria)->queryScalar();
          if ($maxmenuindex)
            $nextmenuindex = $maxmenuindex + 1;

          $model                = new ResourceQuest();
          $model->type          = 'document';
          $model->contentType   = 'text/html';
          $model->published     = 1;
          $model->pub_date      = 0;
          $model->unpub_date    = 0;
          $model->parent        = $this->model->id;
          $model->isfolder      = 0;
          $model->richtext      = 1;
          $model->template      = 2;
          $model->menuindex     = $nextmenuindex;
          $model->uri           = '/quest/';
          $model->searchable    = 0;
          $model->cacheable     = 1;
          $model->deleted       = 0;
          $model->donthit       = 0;
          $model->privateweb    = 0;
          $model->privatemgr    = 0;
          $model->content_dispo = 0;
          $model->hidemenu      = 1;
          $model->class_key     = 'modDocument';
          $model->context_key   = 'web';
          $model->content_type  = 1;
        }
        $ret = $this->saveValidation($model);
        if (isset($ret['ok'])) {
          $this->redirect('/' . $ret['ok']);
          Yii::app()->end();
        }
        else
          $errors .= CVarDumper::dumpAsString($ret, 10, 1);
      }

      $chunks         = preg_split('/\[\/?(password-protection=*[-а-яё\d\w\/: +=&;*,.#]+)?\]/uis', $model->content, -1, PREG_SPLIT_DELIM_CAPTURE);
      $model->content = array_shift($chunks);
      $parts          = array();
      $teams          = array();
      $this->teams_list = Yii::getPathOfAlias('application.runtime') . '/teams-' . $model->id . '.log';
      if(file_exists($this->teams_list) && ($teams_array=parse_ini_file($this->teams_list,true)) !== false) {
        foreach ($teams_array as $team_name=>$props)
          $teams[$props['id']] =
            array('name'=>$team_name,
                  'participants'=>$props['participants'],
                  'code'=>$props['code'],
                  'link'=>'http://'.$_SERVER['HTTP_HOST'].'/'.str_replace('.html','/',$model->uri).$props['code'].'.html');
      }
      foreach ($chunks as $id => $chunk) {
        if (stripos($chunk, 'password-protection=') === 0 && isset($chunks[$id + 1])) {
          $match   = preg_match('/password-protection=([^\/]+)(\/\/([0-9:]+))?(\/\/check)?/i',$chunk,$matches);
          if($match) {
            $password = $matches[1]; $time = (isset($matches[2],$matches[3]))? $matches[3] : ''; $check = isset($matches[4]) ? ' checked="checked"' : '';
            $content  = strip_tags(str_ireplace(array('<br>', '<br/>', '<br />'), "\r\n", str_ireplace('[/password-protection]', '', $chunks[$id + 1])));
            $tips     = array();
            if (preg_match_all('/\[\/?part-tip=?([^\/]+)(\/\/([0-9]+))?\]/uis', $content, $tips_match)) {
              $content = preg_replace('/\[\/?(part-tip=?.*?)\]/uis','',$content);
              foreach($tips_match[1] as $tip_id=>$tip_text) $tips[] = array('text'=>$tip_text,'timeout'=>$tips_match[3][$tip_id]);
            }
            $parts[]  = array('password' =>  $password? $password:'',
                              'time' => $time,
                              'data' => $content,
                              'check'=> $check,
                              'tips' => $tips
            );
          }
        }
      }
      $this->publishAssets(false, array($this->assetsUrl . '/css/forms.css' => 'screen, projection'), array(), array('jquery.ui', 'yiiactiveform','jquery.inputmask','browser','sisyphus'));
      $this->pageTitle  = "Квест: редактирование";
      $this->render('/quest/moderate/editquest', array('model' => $model, 'chunks' => $parts, 'error' => $errors, 'teams'=>$teams));
    }

    public function actionTeamlink() {
      $tname = Yii::app()->request->getPost('team_name');
      $qid   = Yii::app()->request->getPost('team_quest_id');
      if(!empty( $tname) && is_numeric($qid)) {
        $quest = $this->loadQuest($qid);
        $link_code = substr(sha1($tname.date('d-m-Y',$quest['date']).$qid),rand(0,10),10);
        if ($quest) exit(CJavaScript::jsonEncode(array('teamlink'=>'/'.str_replace('.html','/',$quest['uri']).$link_code.'.html','teamlink_code'=>$link_code)));
      }
      exit(CJavaScript::jsonEncode(array('link'=>false)));
    }

    protected function saveValidation($model) {
      $this->checkAccess();

      $saveErrors = CActiveForm::validate($model);
      $this->teams_list = Yii::getPathOfAlias('application.runtime') . '/teams-' . $model->id . '.log';
      $teams = array(); $team_id = 0;
      foreach (Yii::app()->request->getPost('team',array()) as $team_props) {
        if(!isset($team_props['name']) || empty($team_props['name'])) continue;
        $team_id++;
        $teams[] = '['.$team_props['name'].']';
        $teams[] = 'id='.$team_id;
        $teams[] = 'participants="'.$team_props['participants'].'"';
        $teams[] = 'code="'.$team_props['linkcode'].'"';
        $teams[] = "\r\n";
      }
      //CVarDumper::dump($teams,10,1); exit();
      foreach (Yii::app()->request->getPost('parts') as $id => $part) {
        $part['data'] = trim($part['data']);
        $part['text'] = (!empty($part['text']))?trim($part['text']) : 0;
        if (!empty($part['data']) && !empty($part['text'])) {
          $part['time']  = (!empty($part['time']))? $part['time']:'12:00';
          if ($id==0 && $model->pub_date) {
            if(strlen($model->pub_date)==13) $model->pub_date = $model->pub_date/1000;
            else {
              $model->pub_date = strtotime("0:00",$model->pub_date);
            }
            list($starthours,$startmins) = explode(':',$part['time']);
            $model->pub_date += 3600*$starthours+60*$startmins;
          }
          if(!empty($part['text'])) {
            $part['text'] .= '//'.$part['time'];
            if(!empty($part['check'])) $part['text'] .= '//check';
            if(isset($part['tips']) && count($part['tips'])) {
              $tips = '';
              foreach ($part['tips'] as $tipid=>$tipdata) {
                if(!empty($tipdata['text'])) $tips .= '[part-tip='.$tipdata['text'].'//'.$tipdata['timeout'].']';
              }
              $part['data'] = $tips.$part['data'];
            }
          }
          $model->content .= '[password-protection=' . $part['text'] . ']' . $part['data'] . '[/password-protection]';
        }
      }
      if ($saveErrors == '[]') {
        if ($model->save()) {
          if(count($teams)) file_put_contents($this->teams_list,implode("\r\n",$teams),LOCK_EX);
          return array('ok' => $model->uri);
        }
        else return CJavaScript::jsonEncode($model->getErrors());

      }
      else return $saveErrors;
      //Yii::app()->end();
    }


    protected function loadModel($alias) {

      $dependency         = new CDbCacheDependency();
      $dependency->sql    = 'SELECT MAX(IF( editedon >0, editedon, createdon )) FROM modxrev_site_content WHERE uri = :alias';
      $dependency->params = array(':alias' => $alias);

      $model = ResourceQuestBoard::model()->resetScope()->alias('res');
      if (!Yii::app()->user->isAdmin())
        $model->getDbCriteria()->addSearchCondition('published', 1, false, 'AND', '=');

      $model->getDbCriteria()->addSearchCondition('uri', $alias, false, 'AND', '=');
      $model->cache(36, $dependency);
      $model = $model->find();
      if (!$model)
        throw new CHttpException(404, 'Указанная запись не найдена');

      $this->model = $model;

    }

    protected function loadQuests() {

      //выбираем архив квестов
      $quests           = array();
      $questsdependency = new CDbCacheDependency('SELECT MAX( GREATEST( deletedon, editedon, createdon ) )  FROM modxrev_site_content WHERE parent=' . $this->model->id);
      $this->quests     = ResourceArticle::model()->resetScope()->alias('newsres')->withparent($this->model->id)->published()->orderby('publishedon', 'DESC')->cache(36000, $questsdependency)->findAll();

    }


    protected function checkAccess($exit = true) {
      $userFId = Yii::app()->user->getForumId();
      $this->makelog('Квесты, проверка доступа для пользователя: '.Yii::app()->user->name."\nYii::app()->user->getGroups() = ".print_r(Yii::app()->user->getGroups(),1)."\n\$_SESSION['SMFAuth_web_authmember'] = ".@$_SESSION['SMFAuth_web_authmember']."\n\$userFId = ".$userFId."\n",false,'Quest-auth');
      if (!Yii::app()->user->hasAnyGroup(array('Администраторы', '[Консультанты]')) && !in_array($userFId, array(2, 3, 17, 32, 5222)))
        if ($exit)
          throw new CHttpException(404, 'Страница не найдена');
        else return false;
      $this->makelog("ДОСТУП НА МОДЕРАЦИЮ ЕСТЬ\n",false,'Quest-auth');
      return true;
    }

    protected function loadQuest($id) {

      foreach ($this->quests as $quest) {
        if ($quest->id == $id) {
          $model          = $quest;
          $chunks         = preg_split('/(\[password-protection=[-а-яё\d\w\/: +=&;*,.#]+)/uis', $model->content, -1, PREG_SPLIT_DELIM_CAPTURE);
          $model->content = array_shift($chunks);
          $passwords      = array(); $p = 0;
          foreach ($chunks as $id => $chunk) {
            if (stripos($chunk, '[password-protection=') === 0 && isset($chunks[$id + 1])) {
              $p++;
              $passwords[$p] = substr(stristr($chunk, '='), 1);
            }
          }
          return array('uri' => $model->uri, 'passwords' => $passwords, 'date'=>$model->pub_date);
        }
      }
      return false;

    }



  }

  function mb_str_replace($needle, $replacement, $haystack)
  {
      $needle_len = mb_strlen($needle);
      $replacement_len = mb_strlen($replacement);
      $pos = mb_strpos($haystack, $needle);
      while ($pos !== false)
      {
          $haystack = mb_substr($haystack, 0, $pos) . $replacement
                  . mb_substr($haystack, $pos + $needle_len);
          $pos = mb_strpos($haystack, $needle, $pos + $replacement_len);
      }
      return $haystack;
  }


function normalize_str($str) {
  $str = trim(preg_replace('/ +/iu',' ',preg_replace('/[^а-яА-ЯеЁ ]/iu','',mb_str_replace('ё','е',mb_strtolower($str)))));
  return $str;
}
