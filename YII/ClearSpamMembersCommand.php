<?php
/*
* Консольный скрипт очистки базы от спам-регистраций с синхронизацией в СП-базу
* /var/www/yii/protected/yiic clearSpamMembers 2>&1
* @vika.vedy
* 2012-2014
*/

class ClearSpamMembersCommand extends CConsoleCommand
{
    public function run($args) {
      date_default_timezone_set('Asia/Irkutsk');
			set_time_limit(180);
      $starttime = time();
      $spamMebers = SMFMembers::model()->with('ExtendedFields')->unapproved()->findAll();
      $del_ids = array(); $ips = array();
      $ignore_ips = array_filter(file(Yii::getPathOfAlias('application.data').'/ignore_ips.lst',FILE_IGNORE_NEW_LINES));
      if (count($spamMebers)) {
        //$spamMebers = array_slice($spamMebers,0,10);
        echo 'FOUND '.count($spamMebers).' unapproved members'."\n";
        echo 'Ignoring IPs: '.implode($ignore_ips,', ').";\n";
        foreach ($spamMebers as $id=>$mem) {
          $ips[] = $mem->member_ip;
        }
        //echo "\nНайденные IP-адреса: ".print_r(array_unique($ips),1)."\n\n";
        $existenIPsQueryCommand = Yii::app()->dbf->createCommand()
																						->select('`ip`, count(distinct `id_member`) as `count`')
																						->from('smf_log_access_keys')
																						->where(array('in','ip',array_unique($ips)))
																						->group('ip');
        //$existenIPsQueryCommand->prepare();
        //echo "\nЗапрос: ".$existenIPsQueryCommand->getText()."\n\n";

        $existenIPsQuery = $existenIPsQueryCommand->queryAll();
        //echo "\nРезультат: ".print_r($existenIPsQuery,1)."\n\n";
        $existenIPs = array();
        foreach ($existenIPsQuery as $row) {
          $existenIPs[$row['ip']] = $row['count'];
        }
        foreach ($spamMebers as $id=>$mem) {
          $baduser = false;
          $memip   = $mem->member_ip;
          $memext  = $mem->ExtendedFields;
          echo "\n_____________________________________________________________________________________\n".
            ($id+1).': checking ['.$mem->id_member.'] '.$mem->member_name.'…'."\n";
          if (array_key_exists($memip,$existenIPs) && $existenIPs[$memip]>10 && !in_array($memip,$ignore_ips)) {
            echo 'IP '.$memip.' of member «'.$mem->member_name.'» exists in DB, go next'."\n";
            continue;
          }
          else if (count($memext) <=2 ) {
            echo 'DEL: Too little filled fields for member '.$mem->member_name.", checked to DEL\n";
            $baduser = true;
          }
          $extfields = array(); $allextfields = array();
          foreach ($memext as $extfield) {
            $allextfields[$extfield->variable] = $extfield->value;
            if (stristr($extfield->value,'http://')) {
              echo 'DEL: Member '.$mem->member_name.", has link in ".$extfield->variable."! checked to DEL\n";
              $baduser = true;
              break;
            }
            else if (preg_match('/^cust_/',$extfield->variable) && !empty($extfield->value) &&
                     (preg_match('/[а-яА-Я]+/iu',$extfield->value) ||  preg_match('/^89[0-9]{9}$/',$extfield->value))) {
              $extfields[$extfield->variable] = $extfield->value;
              echo 'OK: Member '.$mem->member_name.", has good value for ".$extfield->variable."[".$extfield->value."]! go next check\n";
            }
          }
          if (count($extfields) < 3) {
            echo 'DEL: Too little good addfields for member '.$mem->member_name.", checked to DEL\n";
            $baduser = true;
          }
          if ($baduser) {
            $del_ids[] = $mem->id_member;
            Yii::trace('Deleting: ['.$mem->id_member.'] '.$mem->member_name.":\nattributes=".serialize($mem->attributes)."\next=".serialize($allextfields)."\n",'user.info');
            echo 'DEL: Deleting: ['.$mem->id_member.'] '.$mem->member_name.":\nattributes=".serialize($mem->attributes)."\next=".serialize($allextfields)."\n";
          }
          echo "\n";
        }
      }
      if (count($del_ids)) {
				Yii::trace('Found '.count($del_ids)." users to deleting\n\n\n",'user.info');
				echo 'Found '.count($del_ids)." users to deleting\n\n";

				$url = 'http://somesite/some/api/path/user/delete/id/';
				$waitforreply = 'Delete success';
        foreach ($del_ids as $nid=>$uid) {
					$user = array('id_member'=>$uid);
					$data = base64_encode(serialize($user));
					$hash = md5(md5($uid).'Very_Big_Solt_For_ApI');
					$durl = $url.$uid.'/hash/'.$hash;
        
					$ch = curl_init(); 
					curl_setopt($ch, CURLOPT_URL, $durl); 
					curl_setopt($ch, CURLOPT_HEADER, 0); 
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'data='.rawurlencode($data));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
					$reply=curl_exec($ch);
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
					if($httpCode != 200 || stristr($reply,$waitforreply) === false || $reply === false ) {
						unset($del_ids[$nid]);
						Yii::trace('Cannot delete UID '.$uid." spam registration. \$httpCode = {$httpCode}; curl_error: ".curl_error($ch)."; \$reply = ".strip_tags($reply)."\n\n\n",'user.info');
						echo 'Cannot delete UID '.$uid." spam registration. \$httpCode = {$httpCode}; curl_error: ".curl_error($ch)."; \$reply = ".strip_tags($reply)."\n\n\n";
					}
					else {
						
						Yii::trace('Executed '.$durl." with reply: {$reply}\n",'user.info');
						echo 'Executed '.$durl." with reply: {$reply}\n";
					}
					curl_close($ch);
				}
      }
      if (count($del_ids)) {
				SMFMembers::model()->deleteAll('id_member IN ('.implode(',',$del_ids).')');
				SMFThemes::model()->deleteAll('id_member IN ('.implode(',',$del_ids).')');
				Yii::trace('Deleted: '.count($del_ids)." spam registrations\n\n\n",'user.info');
				echo 'Deleted: '.count($del_ids).' spam-registrations, time: '.round((time()-$starttime)/60,2)." sec.\n\n";
			}
      
      Yii::app()->end();
    }
}


?>
