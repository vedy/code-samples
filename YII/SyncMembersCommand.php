<?php
/*
* Консольный скрипт синхронизации пользовательских профилей
* /var/www/yii/protected/yiic syncMembers 2>&1
* @vika.vedy
* 2012-2014
*/

  class SyncMembersCommand extends CConsoleCommand
  {
      public function run($args) {

      $starttime      = time();
			set_time_limit(180);

      $limit          = 50;
      $users          = array();


      //Выбираем несинхронизированные аватарки
      $sync_avatars_Q  = 'SELECT sm.`id_member` , CONCAT( "http://38mama.ru/forum/uavatars/", sa.filename ) as `avatar`
      FROM `smf_members_sync` AS sm
      LEFT JOIN smf_attachments AS sa ON ( sa.id_member = sm.`id_member` AND sa.attachment_type =1 )
      WHERE ( sm.`avatar` = "" OR sm.`avatar` IS NULL ) AND sm.`id_member` NOT IN ( 2, 3, 17, 850 ) AND sa.filename IS NOT NULL
      ORDER BY sm.`id_member` ';
      $sync_avatars_AR  = SMFSyncMembers::model()->resetScope()->findAllBySql($sync_avatars_Q);
      $modeltransaction = Yii::app()->db->beginTransaction();
      if(count($sync_avatars_AR)) {
        echo 'Найдено '.count($sync_avatars_AR).
          " несинхронизированных аватарок\n———————————————————————————————————————————————————————————————————————————————————————————\n";
        foreach ($sync_avatars_AR as $sync_avatars_Model) {
          echo 'Сохраняем '.$sync_avatars_Model->id_member.': '.$sync_avatars_Model->avatar."\n";
          $sync_avatars_Model->last_update = time();
          $sync_avatars_Model->save();

        }
        unset($sync_avatars_AR);
        $modeltransaction->commit();
        echo "———————————————————————————————————————————————————————————————————————————————————————————\n\n";
        //Yii::app()->end();
      }

      //Ищем обновлённые профили
      $usersAR        = SMFSyncMembers::model()->resetScope()->exclude(array(2,3,17,850))->updated()->limit($limit); //
      $usersCommand   = $usersAR->getCommandBuilder()
                                ->createFindCommand($usersAR->tableSchema, $usersAR->getDbCriteria());
      $users          = $usersAR->findAll();
      $id_members     = CHtml::listData($users,'id_member', 'real_name');

      if(!count($users)) Yii::app()->end();

      echo 'Найдено '.count($users)." обновлённых профилей\n";
      echo 'Выбираем: '.CVarDumper::dumpAsString($id_members)."\n";
      //echo 'Запрос: '.$usersCommand->text."\n";

//       $foreignAQuery =
//         'SELECT `id_member`, `value` FROM `smf_themes` WHERE `id_member` IN ('.implode(',',array_keys($id_members)).') AND `variable`="cust_foreign"';
//       $foreignAr     = SMFThemes::model()->resetScope()->findAllBySql($foreignAQuery);
//       //echo 'Запрос: '.$foreignAQuery."\n";
// 
//       $foreignA = array();
//       foreach ($foreignAr as $foreignArModel) {
//         $foreignA[$foreignArModel->id_member] = $foreignArModel->value;
//       }
//       unset($foreignAr);

      $affected = 0; $queries = 0;
      foreach($users as $userModel) {
        $user = $userModel->attributes;
        $user_identifier = $user['id_member'].' - '.$user['real_name'];
        $user['signature'] = '';
        $user['oldid']     = $user['old_id']?$user['old_id']:$user['id_member'];
        $user['tel']       = $user['phone'];
        $user['old_login'] = mb_strtolower($userModel->member_name,'UTF8');
        $av = $user['avatar'];
//         if($user['old_active'] == 'N' || (isset($foreignA[$user['id_member']]) && $foreignA[$user['id_member']]==1))
//           $user['is_activated'] = 0;
        if(!empty($av) && stristr($av,'http://') === false) $user['avatar'] = 'http://38mama.ru/forum/avatars/'.$user['avatar'];
        else if(empty($av)) {
          $att = SMFAttachments::model()->resetScope()->ofmember($user['id_member'])->avatar()->find();
          if($att) $user['avatar'] = 'http://38mama.ru/forum/uavatars/'.$att->filename;
        }

        unset($user['new_pm'],$user['unread_messages'],$user['posts'],$user['member_name'],$user['real_name'],$user['phone'],$user['old_id'],$user['old_active'],$user['last_update'],$user['last_sync'],$user['last_error'],$user['last_error_time']);
        $data = base64_encode(serialize($user));

        //echo "Отправляем данные: ".CVarDumper::dumpAsString($user)."\n\n";


        if(empty($userModel->real_name)) {//удаляем
          echo '   Удаляем профиль '.$user['id_member'];
          $hash = $this->create_hash($user['id_member']);
          $url  = 'http://somesite/some/api/path/user/delete/id/'.$user['id_member'].'/hash/'.$hash;
          $waitforreply = 'Delete success';
        }
        else {
          echo '   Обновляем профиль '.$user_identifier;
          $hash = $this->create_hash($user['id_member'].$data);
          $url  = 'http://somesite/some/api/path/user/update/id/'.$user['id_member'].'/hash/'.$hash;
          $waitforreply = 'update success';
        }

        echo ': http://sp.38mama.ru/forum/index.php?action=profile;u='.$user['id_member']." \n";


  //      if(@$_SERVER['HOSTNAME']=='larisa.homenet' || @$_SERVER['SERVER_NAME']=='38mama.vvdm') {
  //        echo $url.'<br>data='.rawurlencode($data).'<br>';
  //        continue;
  //      }
        $httpCode = $userReply = null;
        list($httpCode,$userReply) = $this->make_request($url,'unserialize',
                                                         array('postdata'=>'data='.rawurlencode($data)));
        $queries++;
        $userReply = trim($userReply);

        if (stristr($userReply ,'Пользователя не существует')) {
          echo " - Ошибка {$httpCode}, пользователя не существует, пробуем создать…\n\n";

          //возможно, юзера не существует, попробуем создать

          $user['member_name'] = $userModel->member_name;
          $user['real_name']   = $userModel->real_name;
          $data = base64_encode(serialize($user));

          $hash = $this->create_hash($data);
          $url  = 'http://somesite/some/api/path/user/create/hash/'.$hash;
          $waitforreply = 'Success create';
          list($httpCode,$userReply) = $this->make_request($url,'unserialize',
                                                           array('postdata'=>'data='.rawurlencode($data))); $queries++;
          $userReply = trim($userReply);
        }

        if($httpCode != 200 || stristr($userReply,$waitforreply) === false) {
          $userModel->last_error = 'URL: '.$url.'; Code: '.$httpCode.'; reply: '.$userReply.'; sent data: '.$data;
          $userModel->last_error_time = time();
          $userModel->save();
          $userArrStr = CVarDumper::dumpAsString($user);
          echo       <<<END

 ! Ошибка обработки пользователя! ({$httpCode}: {$userReply})

   Отправлены данные:
   $data

   $userArrStr

END;
        }
        else {
          $userModel->last_sync = time();
          $userModel->last_error = new CDbExpression('NULL');
          $userModel->last_error_time = new CDbExpression('NULL');
          $userModel->save();
          $affected++;
          echo " + Пользователь успешно сохранён :) [".trim($userReply)."]\n\n";
        }
      }


      echo '
  ———————————————————————————————————————————————————————————————————————————————————————————
  Сделано '.$queries.' запросов
  Успешно обновлено '.$affected.' записей
  Время выполнения: '.round((time()-$starttime)/60,2).' сек.
  ———————————————————————————————————————————————————————————————————————————————————————————

  ';


      Yii::app()->end();
    }

    private function create_hash($data) {
   		return md5(md5($data).'salt_******************_For_ApI');
   	}

    private function make_request($url,$decode=false,$options = array()) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Opera/12.25 (Windows NT 6.0; U; ru)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        if(isset($options['timeout'])) curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
        if(stristr($url,'/sp.38mama.ru')!==false) {
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
          curl_setopt($ch, CURLOPT_USERPWD, "****:****");
        }
    		if(!isset($options['getheaders'])) curl_setopt($ch, CURLOPT_HEADER, 0);

    		if(isset($options['postdata'])) {
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $options['postdata']);
        }

    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    		$reply    = curl_exec($ch);
    		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    		curl_close($ch);

        switch ($decode) {
          case 'unserialize':
            $userReply = $this->unserialize_me($reply);
            if(!$userReply) $userReply = $reply;
            return array($httpCode,$userReply);
          case 'json_decode':
            return $this->json_decode_me($reply);
          case 'match':
            if(isset($options['regexp'])) {
              $match = preg_match($options['regexp'],$reply,$matches);
              return $matches[1];
            }
          default:
            return str_replace("\xef\xbb\xbf", '', $reply);
        }
      }

    private function json_decode_me($str) {
      return @json_decode(str_replace("\xef\xbb\xbf", '', $str));
    }

    private function unserialize_me($str) {
    		return @unserialize(str_replace("\xef\xbb\xbf", '', $str));
    }
  }

  ?>
