<?php
/*
* Библиотека функций
* @vika.vedy
* 2012-2015
*/

  /**
   * iterate_file      
   * парсинг excel-файла в массив
   * @param string $file
   * @param string $name
   * @param bool $calculate
   *
   * @return array
   */
  function iterate_file ($file,$name,$calculate=false) {
 		global $schema_file;
    error_reporting(E_ALL);
    ini_set('display_errors',1);
    if(isset($_COOKIE['debug'])) tolog('debug: iterate_file('.$file.','.$name.')');

 		$file_data = pathinfo($name);
 		$file_name = str_replace('.'.$file_data['extension'],'',$name);
 		$file_data['extension'] = strtolower($file_data['extension']);
// 		if (!in_array($file_data['extension'],array('xls','xlsx','csv'))) {
//      tolog('iterate_file: <b style="color: red; font-size: 120%">Файл должен быть в формате XLS, XLSX либо CSV. Вы загрузили '.$file_data['extension'].'</b><br>Обработка остановлена');
//      tolog('iterate_file: $file_data = '.var_export($file_data,1),0,0);
//      unlink(scenario_statusfile);
//      return null;
// 		}
 		//echo '$name = '.$name.';$file_data = '.var_export($file_data,true);

 		require_once 'PHPExcel/IOFactory.php';

 		//if($file_data['extension'] == 'csv' && strtolower(mb_detect_encoding(file_get_contents($file))) != 'utf-8') file_put_contents( $file, CP1251toUTF8(file_get_contents($file)) );
 		//copy($file,'temp/utf8-'.$name);

 		//echo "mb_detect_encoding(file_get_contents(".$file.")) =  ".mb_detect_encoding(file_get_contents($file)).'<br />';
 		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
 		PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

 		$objReader = PHPExcel_IOFactory::createReaderForFile($file);
 		if(strtolower($file_data['extension']) != 'csv') { if(!$calculate) $objReader->setReadDataOnly(false); }
 		else $objReader->setInputEncoding('Windows-1251'); //if(strtolower(mb_detect_encoding(file_get_contents($file))) != 'utf-8')
    //$objReader->setOutputEncoding('Windows-1251');

    $cvisible = array();
    $cwidth = array();
    $data = array();
    ini_set('memory_limit','1G');

    //Сокращаем использование памяти: http://stackoverflow.com/questions/4817651/phpexcel-runs-out-of-256-512-and-also-1024mb-of-ram
 		//$objPHPExcel = $objReader->load($file);
    $chunkSize   = 200;
    $max_rows = defined('MAX_ROWS')?MAX_ROWS:65536;
    if(isset($_COOKIE['debug'])) {
      tolog('debug: $max_rows = '.$max_rows);
      tolog('debug: defined(_PARSE_HIDDEN_ROWS) = '.defined('_PARSE_HIDDEN_ROWS'));
    }
    //for ($startRow = 1; $startRow <= $max_rows; $startRow += $chunkSize) {
    //  tolog('<span style="color: #666;">Выбираем из прайса строки с '.$startRow.' по '.($startRow+$chunkSize-1).' из '.$max_rows.'</span>');
        $objPHPExcel = $objReader->load($file);
        unset($objReader);

        $objPHPExcel->setActiveSheetIndex(0);
    		$aSheet = $objPHPExcel->getActiveSheet();
        if ($aSheet->getHighestRow() < 10) {
          $objPHPExcel->setActiveSheetIndex(1);
      		$aSheet = $objPHPExcel->getActiveSheet();
        }
        $aSheetDrawings = array();

        if(defined('EXCEL_IMAGES')) {
          $str_from   = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ь','ы','ъ','э','ю','я',' ','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Х','Ф','Ц','Ч','Ш','Щ','Ь','Ы','Ъ','Э','Ю','Я');
          $str_to     = array('a','b','v','g','d','e','e','j','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','c','s','s','i','i','i','e','u','a','_','a','b','v','g','d','e','e','j','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','c','s','s','i','i','i','e','u','a');
          $str_fromto = array_combine($str_from,$str_to);
          if(strtolower($file_data['extension'])=='xlsx') {
            $collection = $aSheet->getDrawingCollection();
            tolog(__LINE__.'<b style="color:green">Обрабатываем картинки прайса '.$name.'</b> ('.count($collection).')');
            foreach ($collection as $objDrawing) {

              $string = $objDrawing->getCoordinates();
              $coordinate = PHPExcel_Cell::coordinateFromString($string);
              if ($objDrawing instanceof PHPExcel_Worksheet_Drawing){
                $filename0 = basename($name).'.'.str_replace('#xl/media/','',stristr($objDrawing->getPath(),'#xl/media/'));
                $filename  = preg_replace('/[^-a-zA-Z\d.]/ui','',str_ireplace($str_from,$str_to,$filename0));
                tolog(__LINE__.': '.$coordinate[0].$coordinate[1].' Картинка: '.$filename,3);
                $filelink = $objDrawing->getHyperlink();
                if(!$filelink && defined('EXCEL_IMAGES_PATH') && file_exists(EXCEL_IMAGES_PATH) && is_dir(EXCEL_IMAGES_PATH) && is_writeable(EXCEL_IMAGES_PATH)) {
                  if(!file_exists(EXCEL_IMAGES_PATH.$filename)) {
                    tolog('['.$coordinate[0].':'.$coordinate[1].'] Сохраняем фото '.$filename,0,0,0);
                    $zipReader = fopen($objDrawing->getPath(),'r');
                    $imageContents = '';
                    while (!feof($zipReader)) {	$imageContents .= fread($zipReader,1024);	}
                    fclose($zipReader);
                    file_put_contents(EXCEL_IMAGES_PATH.$filename,$imageContents);
                    //tolog('IMG '.$coordinate[0].':'.$coordinate[1].' filename = '.$filename.'; path = '.EXCEL_IMAGES_PATH.$filename.'; exists: '.file_exists(EXCEL_IMAGES_PATH.$filename));
                  }
                  $aSheetDrawings[$coordinate[1]][$coordinate[0]] = array('filename'=>$filename,
                                                                          'description'=>$objDrawing->getDescription(),
                                                                          'file'=>EXCEL_IMAGES_PATH . $filename,
//                                                                          'md5file'=>md5_file(EXCEL_IMAGES_PATH . $filename),
                                                                          'imagesize' => getimagesize(EXCEL_IMAGES_PATH.$filename));
                }
                else if ($filelink)
                  $aSheetDrawings[$coordinate[1]][$coordinate[0]] = array('filename'=>$filename,
                                                                          'link'=>$objDrawing->getHyperlink(),
                                                                          'description'=>$objDrawing->getDescription(),
                                                                          'imagesize' => getimagesize($objDrawing->getPath()));
                else if(defined('EXCEL_IMAGES_PATH')) tolog(__LINE__.' EXCEL_IMAGES_PATH = '.EXCEL_IMAGES_PATH.' not exists or not is writeable!',0,0,0);

              }
            }
          }
          else if (strtolower($file_data['extension'])=='xls') {
            $collection = $aSheet->getDrawingCollection();
            tolog(__LINE__.'<b style="color:green">Обрабатываем картинки прайса '.$name.'</b> ('.count($collection).')');
            foreach ($collection as $objDrawing) {

              $string = $objDrawing->getCoordinates();
              $coordinate = PHPExcel_Cell::coordinateFromString($string);
              if ($objDrawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
                $filename0 = basename($name).'.'.$objDrawing->getIndexedFilename();
                $filename  = preg_replace('/[^-a-zA-Z\d.]/ui','',str_ireplace($str_from,$str_to,$filename0));
                tolog(__LINE__.': '.$coordinate[0].$coordinate[1].' Картинка: '.$filename,3);
                $image    = $objDrawing->getImageResource();
                $renderingFunction = $objDrawing->getRenderingFunction();
                if(defined('EXCEL_IMAGES_PATH') && file_exists(EXCEL_IMAGES_PATH) && is_dir(EXCEL_IMAGES_PATH) && is_writeable(EXCEL_IMAGES_PATH))
                {
                  if(!file_exists(EXCEL_IMAGES_PATH.$filename)) {
                    tolog('['.$coordinate[0].':'.$coordinate[1].'] Сохраняем фото '.$filename,0,0,0);
                    switch ($renderingFunction) {
                      case PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG:
                          imagejpeg($image, EXCEL_IMAGES_PATH . $filename);
                          break;

                      case PHPExcel_Worksheet_MemoryDrawing::RENDERING_GIF:
                          imagegif($image, EXCEL_IMAGES_PATH . $filename);
                          break;

                      case PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG:
                      case PHPExcel_Worksheet_MemoryDrawing::RENDERING_DEFAULT:
                          imagepng($image, EXCEL_IMAGES_PATH . $filename);
                          break;
                    }
                  }
                  $aSheetDrawings[$coordinate[1]][$coordinate[0]] = array('filename'=>$filename,
                                                                          'renderingFunction'=>$renderingFunction,
                                                                          'file'=>EXCEL_IMAGES_PATH . $filename,
//                                                                          'md5file'=>md5_file(EXCEL_IMAGES_PATH . $filename)
                                                                          );
                }
                else if(defined('EXCEL_IMAGES_PATH'))
                  tolog(__LINE__.' EXCEL_IMAGES_PATH = '.EXCEL_IMAGES_PATH.' not exists or not is writeable!',0,0,0);

              }
            }
          }
          $dr_temp = $aSheetDrawings;
          ksort($dr_temp);
          tolog(__LINE__.': iterate_file(): $aSheetDrawings = '.var_export($dr_temp,1),0,0,0);
        }


        tolog(__LINE__.': iterate_file(): getHighestColumn = '.$aSheet->getHighestColumn(),0,0,0);
        tolog(__LINE__.': iterate_file(): getHighestDataColumn= '.$aSheet->getHighestDataColumn(),0,0,0);
    		foreach($aSheet->getRowIterator() as $id => $row) { //получим итератор строки и пройдемся по нему циклом
    			$index   = $row->getRowIndex();
          if(isset($_COOKIE['debug'])) tolog('debug: >getRowIterator(): '.$index);
          if ($index > $max_rows) break;
//          if ($index < $startRow || $index >= ($startRow + $chunkSize)) continue;

    			$visible = $aSheet->getRowDimension($index)->getVisible();
    			//$height  = $aSheet->getRowDimension($index)->getRowHeight();
    			//echo 'Высота строки №'.$index.' = '.$height.'<br />';
    			if(!$visible && !defined('_PARSE_HIDDEN_ROWS')) { continue; }
    			if(!isset($data[$index])) $data[$index] = array();

//          tolog('<span style="color: #666;">Обрабатываем строку '.$index.' </span>',2,0);

    			$lvl = $aSheet->getRowDimension($index)->getOutlineLevel();
    			$cellIterator = $row->getCellIterator(); //получим итератор ячеек текущей строки
    			if(!defined('EXCEL_IMAGES')) $cellIterator->setIterateOnlyExistingCells(true);
//          else $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
    			//$rowbold  = $aSheet->getStyle('A'.$index)->getFont()->getBold();
          $cell_count = 0;
    			foreach($cellIterator as $cell) { //пройдемся циклом по ячейкам строки
            $cell_count++;
//            tolog('debug: >getRowIterator(): '.$index.' $cellIterator $cell '.$cell_count.'',0,0);
    				if (is_null($cell)) {
              tolog('в строке '.$index.' последняя обработанная ячейка: '.$cell_count.'. больше нет контентных ячеек, обработка строки прервана',0,0);
              break; //всё, больше нет контентных ячеек
            }
            $col = $cell->getColumn();
            if(!array_key_exists($col,$cvisible) || !array_key_exists($col,$cwidth)) {
              $cvisible[$col] = $aSheet->getColumnDimension($col)->getVisible();
            }
            if(!$cvisible[$col] && !$calculate && !defined('_PARSE_HIDDEN_ROWS')) {
//              tolog('debug: >getRowIterator(): '.$index.' cell '.$col.' = is not visible!',0,0);
              continue;
            }
            else if(isset($aSheetDrawings[$index][$col])) tolog("isset(\$aSheetDrawings[{$col}][{$index}]) = ".isset($aSheetDrawings[$index][$col]),0,0);
            $value = $calculate? $cell->getCalculatedValue(): $cell->getValue();
            if(!isset($data[$index][$col])) {
              $data[$index][$col] = array();
//              tolog('<span style="color: #666;">новый столбец с данными '.$col.' </span>',4,0);
            }
            $data[$index][$col]['data'] = str_replace("'",'’',trim($value));
            $data[$index][$col]['lvl']  = $lvl;
            if($cell->hasHyperlink())
              $data[$index][$col]['link'] = $cell->getHyperlink()->getUrl();
            $data[$index][$col]['bold']  = $aSheet->getStyle($col.$index)->getFont()->getBold(); //$rowbold
            $data[$index][$col]['size']  = $aSheet->getStyle($col.$index)->getFont()->getSize();
            $data[$index][$col]['color'] = $aSheet->getStyle($col.$index)->getFill()->getStartColor()->getRGB();
            $data[$index][$col]['textcolor'] = $aSheet->getStyle($col.$index)->getFont()->getColor()->getRGB();
            $borders = $aSheet->getStyle($col.$index)->getBorders();
            $data[$index][$col]['borderTop']    = $borders ->getTop()->getBorderStyle();
            $data[$index][$col]['borderBottom'] = $borders ->getBottom()->getBorderStyle();
            $data[$index][$col]['borderRight']  = $borders ->getRight()->getBorderStyle();
            $data[$index][$col]['borderLeft']   = $borders ->getLeft()->getBorderStyle();
//            if(isset($aSheetDrawings[$index]) && isset($aSheetDrawings[$index][$col])) {
//              $data[$index][$col]['image'] = $aSheetDrawings[$index][$col];
//              globallog("\$data[{$index}][{$col}]['image'] = ".var_export($data[$index][$col]['image'],1)."\n\n");
//            }

    			}
          if(isset($aSheetDrawings) && isset($aSheetDrawings[$index])) foreach ($aSheetDrawings[$index] as $col=>$cellimages )
            if (!isset($data[$index][$col]) || !isset($data[$index][$col]['image'])) $data[$index][$col]['image'] = $aSheetDrawings[$index][$col];
          if(isset($_COOKIE['debug'])) tolog('debug: >getRowIterator(): $data['.$index.'] = '.var_export($data[$index],1),0,0,0);
    		}

        //    Free up some of the memory
        $objPHPExcel->disconnectWorksheets();
        unset($objPHPExcel);
    //}
 		return $data;
 	}


	/**
	* curl_download
	*
	*/
  function curl_download($url, $destination, $postdata = null, $headers = array()) {
    global $scenario_tempdir, $chinfo, $curlerror;

    tolog(date('H:i:s').': curl_download ('.$url.', '.$destination.', '.var_export($postdata,1).',  '.var_export($headers,1).')',0,false);

    $fp = fopen($destination, 'w+'); //This is the file where we save the    information
    $ch = curl_init(str_replace(array('+'," "), "%20", $url)); //Here is the file we are downloading, replace spaces with %20

    curl_setopt($ch, CURLINFO_HEADER_OUT, 0);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // to force ipv4

    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true); // не использовать cache
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);


    if (isset($postdata) && !empty($postdata)) {
      curl_setopt($ch, CURLOPT_POST, true);
      $post = is_array($postdata) ? http_build_query($postdata, null, '&') : $postdata;
      if (defined('MULTIPART') && is_array($postdata)) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        tolog('Метод запроса: POST MULTIPART, передаваемые данные: ' . var_export($postdata, 1) . '; $url=' . $url, 5, false);
      }
      else {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        tolog('Метод запроса: POST, передаваемые данные: ' . $post . '; $url=' . $url, 5, false);
      }
    }
    if (defined('CURLOPT_VERBOSE'))
      curl_setopt($ch, CURLOPT_VERBOSE, 1);

    $hrs[0] = 'Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4';
    $hrs[]  = 'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
    $hrs[]  = 'Accept-Charset:utf-8';
    $hrs[]  = 'Accept-Encoding:gzip, deflate, sdch';
    $hrs[]  = 'Connection:keep-alive';
    $hrs[]  = 'DNT:1';
    if (isset($headers) && is_array($headers)) {
      foreach ($headers as $name => $value) {
        $hrs[] = $name . ':' . $value;
      }
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, $hrs);

    if (isset($_SESSION['set_ua'])) {
      curl_setopt($ch, CURLOPT_USERAGENT, $_SESSION['set_ua']);
      tolog('User Agent: ' . $_SESSION['set_ua'], 5, false);
    }
    else curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Win x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36');

    curl_exec($ch); $chinfo = curl_getinfo($ch); $curlerror = curl_error($ch);
    curl_close($ch);
    fclose($fp);
    return filesize($destination);
  }

  function get_img_by_url($src,$scenario) {
  		global $valid_images, $readypics;
  		if(!empty($readypics[$src])) return $readypics[$src];
  		$picname  = preg_replace('/[^-._a-zA-Z0-9]/','',basename($src));
  		$tmpfname = $_SERVER['DOCUMENT_ROOT'].'/parser/scenarious/'.$scenario.'/pic/'.$picname;
  		if(file_exists($tmpfname) && filesize($tmpfname)) {$readypics[$src] = $picname; return $picname;}

  		$ch = curl_init($src);

  		$fp = fopen("$tmpfname", "w");
  		curl_setopt($ch, CURLOPT_FILE, $fp);
  		curl_setopt($ch, CURLOPT_HEADER, 0);
  		curl_setopt($ch, CURLOPT_REFERER, $src);
  		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; WOW64; Trident/4.0; AEEA0D84-5CBE-2945-EB23-712BCD8E92F8; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; SLCC1; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; OfficeLiveConnector.1.4; OfficeLivePatch.1.3)');
  		//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1); // не использовать cache
  		$hrs[0]='Accept-Language:ru,en-us;q=0.7,en;q=0.3';
  		$hrs[1]='Accept-Charset:windows-1251,utf-8;q=0.7,*;q=0.7';
  		$hrs[2]='Keep-Alive:300';
  		$hrs[3]='Connection:keep-alive';
  		curl_setopt($ch, CURLOPT_HTTPHEADER, $hrs);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  		curl_exec($ch);
  		fclose($fp);
  		curl_close($ch);

  		if(!filesize($tmpfname) || !in_array(get_mimetype($tmpfname),$valid_images)) unlink($tmpfname);
  		$readypics[$src] = $picname;
  		return (file_exists($tmpfname) && filesize($tmpfname))? $picname : null;
  };

  function get_thumb($src,$scenario) {
    $faddr = '/sp/parser/scenarious/screens/'.$scenario.'.png';
    $fpath = $_SERVER['DOCUMENT_ROOT'].$faddr;
    $dumbthumbpath = $_SERVER['DOCUMENT_ROOT'].'/sp/parser/we-will-be-back-soon-sticky-note.png';
    $cached_filename       = str_replace('.png','.orig.png',$fpath);
    $cached_filename_back  = str_replace('.png','.back.png',$fpath);
    $fmd5  = file_exists($fpath)?md5_file($fpath):false;
    $thumb_small_width  = 300;	//	small image width
    $thumb_small_height = 250;	//	small image height
    $execoutput = array();

    if(!file_exists($fpath) || !filesize($fpath) ) {
      if(file_exists($fpath)) {
        rename($fpath,$cached_filename_back);
      }
      globallog('Запрашиваем скриншот для сайта '.$src.', $faddr = '.$faddr);
      if(preg_match('/[а-я]+/ui',$src)) {
        require_once('idna_convert.class.php');
        $idn = new idna_convert(array('idn_version'=>2008));
        $src = $idn->encode($src);
      }
      $fimg_url = 'xvfb-run --server-args="-screen 0, 1024x750x24" cutycapt --url=\''.$src. '\' --min-width=1024 --out=\''.$cached_filename.'\' 2>&1';
      //if(($fmd5 && $fmd5!='56a1051a45713d2d6aff5830959e08b5') || (file_exists($fpath) && filemtime($fpath) < time()-30*24*3600))
      //  $fimg_url .= '&refresh=1';
      //@globallog('get_thumb('.$src.','.$scenario.') $fimg_url = '.$fimg_url);
      ini_set('memory_limit', '1024M');
      $fimg = @exec($fimg_url,$execoutput);
      if(file_exists($cached_filename)) {
        list($orig_x, $orig_y, $orig_img_type, $img_sizes) = GetImageSize($cached_filename);
        $org_image   = ImageCreateFromPng($cached_filename);
        $orig_y_S = ($orig_x/$thumb_small_width) * $thumb_small_height;
        if($orig_y_S>$orig_y){
          $thumb_small_height = $thumb_small_height * ($orig_y/$orig_y_S);
          $orig_y_S = $orig_y;
        }
        $new_image   = ImageCreateTrueColor($thumb_small_width,$thumb_small_height);
        imagecopyresampled(
            $new_image, // Destination image link resource.
            $org_image, // Source image link resource.
            0, 0,       // destination point.(x,y)
            0, 0,       // source point.(x,y)
            $thumb_small_width, $thumb_small_height,   // Destination (width,height).
            $orig_x, $orig_y_S
        );  // Source (width,height)
        $res = ImagePNG($new_image,  $fpath);
        if($res && file_exists($cached_filename_back)) unlink($cached_filename_back);
      }
      else globallog('get_thumb error! $fimg = '.$fimg.'; $execoutput='.var_export($execoutput,1)."\r\n\r\n");

      if(file_exists($cached_filename_back)){
        rename($cached_filename_back,$fpath);
      }
      else if(!file_exists($fpath)) {
        copy($dumbthumbpath,$fpath);
      }
    }
    return $faddr;
  }

  function url_exists($url) {
      $resURL = curl_init();
      curl_setopt($resURL, CURLOPT_URL, $url);
      curl_setopt($resURL, CURLOPT_BINARYTRANSFER, 1);
      curl_setopt($resURL, CURLOPT_HEADERFUNCTION, 'curlHeaderCallback');
      curl_setopt($resURL, CURLOPT_FAILONERROR, 1);

      curl_exec ($resURL);

      $intReturnCode = curl_getinfo($resURL, CURLINFO_HTTP_CODE);
      curl_close ($resURL);

      tolog('url_exists('.$url.') $intReturnCode = '.$intReturnCode,0,0,0);
      if ($intReturnCode != 200 && $intReturnCode != 302 && $intReturnCode != 304) {
         return false;
      }
      else {
         return true ;
      }
  }

  function url_exists_alt($file) {
    $file_headers = @get_headers($file);
    tolog('url_exists_alt('.$file.') headers[0] = '.var_export($file_headers[0],true).'; will return '.((mb_stristr($file_headers[0],'404')!==false)?'false':'true'),0,0,0);
    if(mb_stristr($file_headers[0],'404')!==false)
      return false;
    else
      return true;
  }

  function logerror($txt) {

    if(is_array($txt)) $txt = nl2br(var_export($txt,true));
    file_put_contents(scenario_errlogfile, date('[d-m-Y H:i:s] ').$txt."\r\n",FILE_APPEND);
    flush();

  }

  function logpc($percent=false) {
    if(!$percent && isset($_SESSION[scenario_name.'percent'])) $percent = $_SESSION[scenario_name.'percent'];
    return $percent? $percent:0;
  }

  function globallog($str,$error=false) {
    @file_put_contents($error?_globalerrfile:_globallogfile, date('[d-m-Y H:i:s] ').$_SERVER['REMOTE_ADDR'].' '.$str."\r\n",FILE_APPEND);
  }
  
  function tolog_obj($obj) {
  	$fd = fopen(scenario_logfile, 'a+');
  	if ($fd) {
  		fwrite($fd, date('[d-m-Y H:i:s] ').$_SERVER['REMOTE_ADDR'].' '.logpc().'% '. print_r($obj, true) . "\r\n");
  		fclose($fd);
  	}
  }

  function tolog($str,$lvl=0,$show=true,$unhtml=true,$force=false) {
    if(!isset($_REQUEST['preloadaction']) && !$force) check_status(true);
    if(is_array($str)) $str = nl2br(var_export($str,true));
    if($show) {
      file_put_contents(
        scenario_htmllogfile,
        '<div name="logline" class="lvl'.$lvl.'" p="'.logpc().'"><i id="tm" class="tms">'.time().'</i><span class="str">'.str_replace(array("\r","\n"),'',$str).'</span></div>'."\r\n",
        FILE_APPEND);

    }

//    $pc = str_pad(logpc(),3,0,STR_PAD_LEFT);
    if($unhtml) {
      $str = str_replace('<br />', ' | ',$str);
      $str = @strip_tags($str);
      file_put_contents(scenario_logfile, date('[d-m-Y H:i:s] ').$_SERVER['REMOTE_ADDR'].' '.logpc().'% '.$str."\r\n",FILE_APPEND);
    }
    else
      file_put_contents(scenario_logfile, date('[d-m-Y H:i:s] ').$_SERVER['REMOTE_ADDR'].' '.logpc().'% '.$str."\r\n\r\n\r\n",FILE_APPEND);
  };

  function remove_contents($url) {
    global $scenario_tempdir;
    if(!empty($url) && file_exists($scenario_tempdir.md5($url))) unlink($scenario_tempdir.md5($url));
  }

  function clean_utf8 (&$str) {
//    $regex = "/[^-  0-9a-zA-zа-яеёА-ЯЕЁъЪ\/*\[\].,;:~!@#$%^&()+=_—`'\\\]/iu";
//    $str = preg_replace($regex, '', $str);

    $regex = <<<'END'
/
  (
    (?: [\x00-\x7F]
    |   [\xC0-\xDF][\x80-\xBF]
    |   [\xE0-\xEF][\x80-\xBF]{2}
    |   [\xF0-\xF7][\x80-\xBF]{3}
    ){1,120}
  )
| .
/x
END;

    $str = preg_replace($regex, '$1', $str);
    return $str;

  }

  if (!function_exists('json_last_error_msg')) {
      function json_last_error_msg() {
          static $errors = array(
              JSON_ERROR_NONE             => 'Нет ошибок',
              JSON_ERROR_DEPTH            => 'Достигнута максимальная глубина стека',
              JSON_ERROR_STATE_MISMATCH   => 'Некорректные разряды или не совпадение режимов',
              JSON_ERROR_CTRL_CHAR        => 'Некорректный управляющий символ',
              JSON_ERROR_SYNTAX           => 'Синтаксическая ошибка, некорректный JSON',
              JSON_ERROR_UTF8             => 'Некорректные символы UTF-8, возможно неверная кодировка'
          );
          $error = json_last_error();
          return array_key_exists($error, $errors) ? $errors[$error] : "Unknown error ({$error})";
      }
  }

  function in_cache($url,$days = 1) {
    global $scenario_tempdir,$chinfo, $curlerror;
    $page_temp_path = $scenario_tempdir.md5($url);
    return !(!file_exists($page_temp_path) || !$days || filectime($page_temp_path) < time()-$days*24*3600 || (filesize($page_temp_path)<1024 && (!defined('SMALL_DATA') || filesize($page_temp_path)<SMALL_DATA) ));
  }

  function get_contents($url,$days = 1,$timeout=true,$postdata=null,$headers=null,$minsize=1024) {
    global $scenario_tempdir,$chinfo, $curlerror;

    $minsize = defined('MIN_LENGTH')? MIN_LENGTH : $minsize;
//    tolog('> get_contents $minsize = '.$minsize,0,0,0);

    $enc = (!defined('ENCODING'))?'utf8':ENCODING;
    $count = defined('TRY_TIMES')? TRY_TIMES: 3;
    for($i = 1; $i<=$count; $i++) {
      if($i>1 && !$timeout) {
        if($i>2 && !defined('CURLOPT_VERBOSE')) define('CURLOPT_VERBOSE',1);
        $timeout = true;
      }
      switch ($enc) {
        case '1251':
        case 'windows-1251':
        case 'windows 1251':
        case 'Windows-1251':
        case 'Windows 1251':
        case 'Windows1251':
        case 'Win1251':
        case 'CP1251':
        case 'CP-1251':
          $contents = get_contents_1251($url,$days,$timeout,$postdata,$headers,$minsize);
          break;
        case 'koi8':
        case 'koi-8':
        case 'koi8-r':
        case 'koi-8-Ru':
          $contents = get_contents_koi8($url,$days,$timeout,$postdata,$headers,$minsize);
          break;
        default:
          $contents = get_contents_utf8($url,$days,$timeout,$postdata,$headers,$minsize);
      }
      $size = mb_strlen($contents);
      if($size >= $minsize) $i = $count+1;
      else if ($i == $count) tolog(__LINE__.': Пустая страница по ссылке: $url '.$url.'; $minsize = '.$minsize.'; $count = '.$count.'; $curlerror='.$curlerror.'; $chinfo = '.var_export($chinfo,1),0,0,0);
    }
    return $contents; //preg_replace('/<(\/?)noindex/uis','<\1div',$contents);

  }


  function get_contents_utf8($url,$days = 1,$timeout=true,$postdata=null,$headers=null,$minsize=1024) {
  		global $scenario_tempdir, $chinfo, $curlerror;
  		$page_temp_path = $scenario_tempdir.md5($url);
      $minsize = defined('SMALL_DATA')? SMALL_DATA : $minsize;
//      tolog('> get_contents_utf8 $minsize = '.$minsize,0,0,0);
  		if (!file_exists($page_temp_path) || !$days || filectime($page_temp_path) < time()-$days*24*3600 || filesize($page_temp_path)==0 || filesize($page_temp_path)<$minsize )  {
        $timeoutmax = defined('_TIMEOUT')?_TIMEOUT*1000000 : 3000000;

//        tolog(date('H:i:s').': get_contents file_exists('.$page_temp_path.') = '.file_exists($page_temp_path),5,false);

        if($timeout) {
          $tm_value = rand(500000,$timeoutmax);
          tolog(date('H:i:s').': get_contents таймаут '.($tm_value/1000000).' sec. ('.$url.', '.$days.', '.$timeout.',  '.$minsize.'); defined(\'_COOKIEFILE\') = '.defined('_COOKIEFILE'),5,false);
          usleep($tm_value);
        }
        else tolog(date('H:i:s').': get_contents ('.$url.', '.$days.', '.$timeout.',  '.$minsize.'); defined(\'_COOKIEFILE\') = '.defined('_COOKIEFILE'),5,false);

  			$lu = (file_exists($page_temp_path))? date('d-m-Y H:i:s',filemtime($page_temp_path)) : 0;
        if (file_exists($page_temp_path) ) unlink($page_temp_path);

        if(defined('USE_PROXY')) {
          $proxy_url = 'http://lmoroz.tmweb.ru/p/browse.php?u='.urlencode($url);
          tolog('USE PROXY! '.$proxy_url,0,0,0);
          $ch = curl_init($proxy_url);
        }
  			else $ch = curl_init($url);

  			$fp = fopen($page_temp_path, "w");

        curl_setopt($ch, CURLINFO_HEADER_OUT, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // to force ipv4

        if(defined('_COOKIEFILE')) {
          tolog('CURLOPT_FOLLOWLOCATION enabled. Используем cookie из файла '._COOKIEFILE.'; $url='.$url,5,false);
          curl_setopt($ch, CURLOPT_COOKIEFILE, _COOKIEFILE);
          curl_setopt($ch, CURLOPT_COOKIEJAR, _COOKIEFILE);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }
        if (isset($postdata)) {
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
          //curl_setopt($ch, CURLOPT_HEADER, true);

          $post = is_array($postdata)?http_build_query($postdata,null,'&'):$postdata;
          if(defined('MULTIPART') && is_array($postdata)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            tolog('Метод запроса: POST MULTIPART, передаваемые данные: '.var_export($postdata,1).'; $url='.$url,5,false);
          }
          else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            tolog('Метод запроса: POST, передаваемые данные: '.$post.'; $url='.$url,5,false);
          }
        }
				if(defined('FOLLOWLOCATION')) curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				if(defined('CURLOPT_VERBOSE')) curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_ENCODING, true);
  			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  			curl_setopt($ch, CURLOPT_FILE, $fp);
  			curl_setopt($ch, CURLOPT_REFERER, $url);

  			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true); // не использовать cache
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

  			$hrs[0]='Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4';
  			$hrs[]='Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        $hrs[]='Accept-Charset: utf-8';
  			$hrs[]='Accept-Encoding: gzip, deflate, sdch';
  			$hrs[]='Connection: keep-alive';
  			$hrs[]='DNT: 1';
        if(isset($headers) && is_array($headers)) {
          foreach ($headers as $name=>$value) {  $hrs[]=$name.': '.$value; }
//          tolog(__LINE__.':  get_contents_utf8 CURLOPT_HTTPHEADER = '.var_export($hrs,1),0,0,0);
        }
  			curl_setopt($ch, CURLOPT_HTTPHEADER, $hrs);

  			if(isset($_SESSION['set_ua'])) {
          curl_setopt($ch, CURLOPT_USERAGENT, $_SESSION['set_ua']);
          tolog('User Agent: '.$_SESSION['set_ua'],5,false);
        }
  			else curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Win x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36');

  			if(!empty($_SESSION['set_cookies'])) {
  				curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['set_cookies']);
  				tolog('<small>get_contents: Работаем с куками: '.$_SESSION['set_cookies'].'</small>',1,false);
  			}

        //tolog('curl_exec('.$url.')',1,false);
  			curl_exec($ch); $chinfo = curl_getinfo($ch); $curlerror = curl_error($ch);
  			fclose($fp);
  			curl_close($ch);

  			tolog(date('H:i:s').': Выбрано содержимое со страницы: '.$url.' ('.strlen(file_get_contents($page_temp_path)).' b) в файл: '.md5($url).($lu?'<br /><span class="small">посл. обновление: '.$lu.'</span>':''),5,false);
  		}
  		//else touch($page_temp_path); //меняем время модификации файла, чтобы при обходе чистильщиком он не был удалён
  			//tolog('get_contents from file: '.md5($url).'  <span class="small">('.round(filesize($page_temp_path)/1024).' Kb, '.date('d-m-Y h:i:s',filemtime($page_temp_path)).')</span>',5,false);


  		return file_get_contents($page_temp_path);
  }

  function get_contents_1251($url,$days = 1,$timeout=true,$postdata=null,$headers=null,$minsize=1024) {
    global $scenario_tempdir,$chinfo, $curlerror;
    $minsize = defined('MIN_LENGTH')? MIN_LENGTH : $minsize;
    $count   = defined('TRY_TIMES')? TRY_TIMES: 3;
    for($i = 1; $i<=$count; $i++) {
      if($i>1 && !$timeout) $timeout = true;
      $contents = str_ireplace('windows-1251','UTF-8',iconv('Windows-1251','UTF-8//IGNORE',get_contents_utf8($url,$days,$timeout,$postdata,$headers,$minsize)));
      if(mb_strlen($contents) >= $minsize) $i = $count+1;
      else tolog(__LINE__.': Пустая страница по ссылке: $url '.$url.'; $minsize = '.$minsize.'; $count = '.$count.'; $curlerror='.$curlerror.'; $chinfo = '.var_export($chinfo,1),0,0,0);
    }
    return $contents;
  }

  function get_contents_koi8($url,$days = 1,$timeout=true,$postdata=null,$headers=null,$minsize=1024) {
    global $scenario_tempdir,$chinfo, $curlerror;
    $minsize = defined('MIN_LENGTH')? MIN_LENGTH : $minsiz;
    $count = defined('TRY_TIMES')? TRY_TIMES: 3;
    for($i = 1; $i<=$count; $i++) {
      if($i>1 && !$timeout) $timeout = true;
      $contents = str_ireplace(array('koi8-r','windows-1251','CP-1251','CP1251','KOI-8','Windows1251'),'UTF-8',iconv('koi8-r','UTF-8',get_contents_utf8($url,$days,$timeout,$postdata,$headers,$minsize)));
      if(mb_strlen($contents) >= $minsize) $i = $count+1;
      else tolog(__LINE__.': Пустая страница по ссылке: $url '.$url.'; $minsize = '.$minsize.'; $count = '.$count.'; $curlerror='.$curlerror.'; $chinfo = '.var_export($chinfo,1),0,0,0);
    }
    return $contents;
  }

  function check_status($exit=false) {
      if(defined('scenario_statusfile') && file_exists(scenario_statusfile)) {
        $stat_time = file_get_contents(scenario_statusfile);
        if($stat_time && (int)$stat_time < strtotime('-14 hours') && !local_debug) {
          //если файл статуса не обновлялся дольше 2 часов, считаем, что обработка по каким-то причинам прервана
          logerror('Обнаружен устаревший файл статуса ('.date('Y-m-d h:i:s',(int)$stat_time).' / '.$stat_time.'). Файл удалён. Обработка остановлена.');
          unlink(scenario_statusfile);
          if ($exit) exit();
        }
      }
      if((!defined('scenario_statusfile') || !file_exists(scenario_statusfile)) && $exit) exit();
  }


  function get_matched($regex,$content,$names=array(),$all=false, $glue='') {
  		$matches = false;
  		$match   = $all ? preg_match_all($regex,$content,$matches,PREG_SET_ORDER): preg_match($regex,$content,$matches);
  		if (!$match) return false;
  		if (!isset($names) || !is_array($names) || !count($names)) {
  			if (!$all) {
          array_shift($matches);
          return (count($matches) == 1)? $matches[0] : ((!empty($glue))? implode($glue,$matches) : $matches);
        }
  			$only_matches = array();
  			foreach ($matches as $match_item_id => $match_item_array) {
  				array_shift($match_item_array);
  				if (count($match_item_array) == 1) $only_matches[] = $match_item_array[0];
  			}
  			if(count($only_matches)) return (!empty($glue))? implode($glue,$only_matches) : $only_matches;
  			return $matches;
  		}
  		if(!$all) {array_shift($matches); $matches = @array_combine($names,$matches);}


  		else {
  			foreach ($matches as $match_item_id => $match_item_array) {
  				array_shift($match_item_array);
  				$matches[$match_item_id] = array_combine($names,$match_item_array);
  			}
  		}
  		return $matches;
  }

  if (!function_exists("mb_trim")) {
    function mb_trim($string, $trim_chars = '\s'){
      return preg_replace('/^['.$trim_chars.']*(?U)(.*)['.$trim_chars.']*$/u', '\\1',$string);
    }
  }

  function to_utf8($str) {
    $str = mb_convert_encoding($str,'UTF-8','Windows-1251');
    return $str;
  }

  function trim_html($str) {
    return mb_trim(str_replace(array('&laquo;','&raquo;'),array('«','»'),str_replace(': * ',': ',preg_replace('/\s+/ui',' ',preg_replace("/(\s+\n+\s+)+/iu"," * ",trim(strip_tags(str_replace(array("/",' ',"&amp;"),array("-",' ','&'),str_replace(array('"',"<br>",'&quot;'),"",str_replace(array("\n",'<br>','<br />'),' ',$str)))))))))," \n\t*;:., .");
  }

  function remove_quotes($str,$save_slash = false) {
    $str = mb_trim(mb_trim(str_replace(array('&laquo;','&raquo;'),array('«','»'),preg_replace('/\s+/uis',' ',str_replace(array('"','&quot;','\\"'),"",str_replace(array("&amp;","\r","\n",' '),array("&",'',' ',' '),strip_tags($str))))))," \n\t;:.,-");
    return $save_slash? $str : mb_trim(str_replace('/','-',$str)," \n\t*;:.,-");
  }

  if(!function_exists('mb_ucfirst')) {
      function mb_ucfirst($str, $enc = 'UTF-8',$onlyupper=false) {
        if(empty($enc)) $enc = 'UTF-8';
        $str = $onlyupper?
          mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc) :
          mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_strtolower(mb_substr($str, 1, mb_strlen($str, $enc), $enc), $enc);
      		return $str;
      }
  }

  if(!function_exists('to_cp1251')) {
    function to_cp1251(&$str,$key=null,$convert=true) {

      $orig_str = $str;
      if ($convert) {
        $str = mb_trim(iconv('UTF-8', 'Windows-1251//TRANSLIT', $orig_str));
        if(empty($str) && $str!==0) {
//          error_log('ICONV error!: '.$orig_str.' = '.$str);
          $str = mb_convert_encoding($orig_str,'Windows-1251','UTF-8');
        }
      }
      return $str;
    }
  }

  /**
   * site_sort: сравнение строк в массиве
   * @param array $a ('website'=>'http://aaa.com')
   * @param array $b ('website'=>'http://bbb.com')
   *
   * @return int
   */
  function site_sort($a, $b) {
    return strcmp($a['website'],$b['website']);
  }

  function populate_rows ($rows_array) {
    $ret = 'r=1';
    foreach ($rows_array as $k=>$v) {
      $ret.='&rname%5B%5D='.trim($k).'&rval%5B%5D='.trim($v);
    }
    return $ret.'&rname%5B%5D=%D0%9D%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5&rval%5B%5D=%D0%97%D0%BD%D0%B0%D1%87%D0%B5%D0%BD%D0%B8%D0%B5';
  }


  function get_file($e) {
    global $scenario_site;
    $scenario_site = $scenario_site?$scenario_site:(SITE_URL?SITE_URL:'-');
 		$Q = $_FILES[$e];

    error_reporting(E_ALL);
    ini_set('display_errors',1);

    if (!$Q ){
      tolog('<b style="color: red; font-size: 120%">Не удалось сохранить загруженный файл<br>Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return null;
    }


    if(!is_array($Q["name"])) {
      foreach ($Q as $data_name=>$data_value) $Q[$data_name] = array($data_value);
      $count = 1;
      $multiple = false;
    }
    else {
      $count = count($Q["name"]);
      $multiple = true;
    }

 		for ($i=0; $i<$count; $i++) {
      if ( $Q["error"][$i] ) {
        continue;
        tolog('<b style="color: red;"> Ошибка сохранения файла «'.$Q["name"][$i].'»: '.$Q["error"][$i].'</b>');
        //unlink(scenario_statusfile);
        //return null;
      }
      $mime_type = strstr(trim ( exec ('file -bi ' . escapeshellarg ( $Q["tmp_name"][$i] ) ) ),';',1);
      tolog('<b style="color: blue">Получен файл (тип: '.$mime_type.') '.$Q["name"][$i].'</b>');

      $valid_types = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel','text/csv','application/xls','application/vnd.ms-office','application/ms-excel','text/plain','application/msexcel','application/x-excel');
//      if(!in_array($mime_type,$valid_types) && !in_array($Q['type'][$i],$valid_types)) {
//        tolog('<b style="color: red; font-size: 120%">Файл должен быть в формате XLS, XLSX либо CSV Вы загрузили '.$mime_type.' ('.$Q['type'][$i].')</b><br>Обработка остановлена');
//        unlink(scenario_statusfile);
//        return null;
//      }


      if(!copy($Q["tmp_name"][$i],scenario_tempdir.'/original-'.$Q["name"][$i])) {
         tolog('<b style="color: red; font-size: 120%">Не удалось сохранить оригинальную версию файла</b><br>Обработка остановлена');
         unlink(scenario_statusfile);
         return null;
      };
      $multiple?
        $_SESSION['load_file'][scenario_name][$i] = array('path'=>scenario_tempdir.'/original-'.$Q["name"][$i],'name'=>$Q["name"][$i]):
        $_SESSION['load_file'][scenario_name] = array('path'=>scenario_tempdir.'/original-'.$Q["name"][$i],'name'=>$Q["name"][$i]);


      tolog('<span style="color: blue">Данные записаны в сессию: '.$Q["name"][$i].'</span>');
    }
    if(isset($_COOKIE['debug'])) tolog('debug: <span style="color: red">Включен режим отладки!</span>');
 		return $_SESSION['load_file'][scenario_name];
 	}


  function iterate_csv_file ($file,$name) {
    global $schema_file;
    ini_set('memory_limit','1G');
    if(!defined('CSV_DELIMITER')) define('CSV_DELIMITER',',');

    if(($fp = fopen($file,'r')) == false) {
      tolog('<b style="color: red; font-size: 120%">Не удалось прочитать загруженный файл '.$name.'</b><br>Обработка остановлена');
      unlink(scenario_statusfile);
      return null;
    };
    $data = array(); $keys = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    $strnum=0;
    while($csv_line = fgetcsv($fp,1024,CSV_DELIMITER)) {
      if(count($csv_line) > 26) continue;
      $strnum++;
      $csv_line = array_map("to_utf8",$csv_line);
      $cur_keys = array_slice($keys,0,count($csv_line));
      $data[$strnum] = array_combine($cur_keys,$csv_line);
      //tolog('<span style="color: #666">Разбираем строку '.$strnum.', найдено '.count($csv_line).' полей: '.var_export($data[$strnum],1).'</span>'); flush();
    }
    fclose($fp);
    return $data;
  }

  function iterate_html_file($file,$name) {
    global $schema_file;
    error_reporting(E_ALL);
    ini_set('display_errors',1);
    if(isset($_COOKIE['debug'])) tolog('debug: iterate_html_file('.$file.','.$name.')');

    $data = array(); $keys = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    $file_data = pathinfo($name);
    $file_name = str_replace('.'.$file_data['extension'],'',$name);
    $file_data['extension'] = strtolower($file_data['extension']);

    require_once (dirname(__FILE__).'/phpQuery-onefile.php');

    tolog('Проходимся по строкам файла '.$name,1);
    $content = str_replace('WINDOWS-1251','UTF-8',file_get_contents($file));
    $content = mb_convert_encoding($content,'UTF-8','Windows-1251');
    phpQuery::newDocumentHTML($content);
    tolog('Файл '.$name.' загружен в парсер',1);
    $rows_data = pq('tr');
    $strnum=0;
    tolog('Всего найдено строк: '.count($rows_data),1);
    foreach ($rows_data as $row) {
      $tds = pq('td',$row);
      $tds_data = array();
      foreach($tds as $td) $tds_data[] = $td->nodeValue;
      if(!count($tds_data)) continue;
      $strnum++;
//      $tds_data = array_map("to_utf8",$tds_data);
//      tolog($strnum.': <i>'.implode(' | ',$tds_data).'</i>',2);
      $cur_keys = array_slice($keys,0,count($tds_data));
      $data[$strnum] = array_combine($cur_keys,$tds_data);
    }
    unset($content);
    unset($rows_data);
    phpQuery::unloadDocuments();
    return $data;
  }

  function array_slice_by_value (&$array, $startkey, $endkey, $preserve_keys = true)
  {
     if(!isset($startkey)) return array();
     $offset = array_search($startkey, $array);

     if (is_string($endkey))
       $endkey = array_search($endkey, $array) - $offset + 1;

     return array_slice($array, $offset, $endkey, $preserve_keys);
  }

  function make_full_url($url,$site_url=false) {
    if(!$site_url && !defined('SITE_URL')) return false;
    $site_url = $site_url?$site_url:SITE_URL;
    return empty($url)? false : ( (preg_match('~^https?://~',$url))?$url:( (preg_match('~^//~',$url))?'http:'.$url : ( (preg_match('~^/~',$url))?$site_url.$url : $site_url.'/'.$url) ) );
  }


  function fatal_handler() {
    $errfile = "unknown file";
    $errstr  = "shutdown";
    $errno   = E_CORE_ERROR;
    $errline = 0;

    $error = error_get_last();

    if( $error !== NULL) {
      $errno   = $error["type"];
      $errfile = $error["file"];
      $errline = $error["line"];
      $errstr  = $error["message"];
    }
    tolog('Fatal error! '.$errno.' '.$errstr.' in '.$errfile. ' on line '.$errline,0,false);
  }

  function write_rows() {
    global $rows_array, $show_errors, $show_messages, $scenario_rowsfile;


    $scenario_rowsfile = isset($scenario_rowsfile)? $scenario_rowsfile : scenario_rowsfile;
    $error = false; $cnt = count($rows_array);
    tolog('Сбрасываем кеш товаров с рядами: '.$cnt,0,0,0);
    if($cnt >2000) {
      $chunks = array_chunk($rows_array,1500,true);
      tolog('Количество частей: '.count($chunks),0,0,0);
      foreach ($chunks as $id=> $rows_array_chunk) {
        tolog('$rows_array_chunk №'.($id+1).' = '.@var_export($rows_array_chunk,1),0,false);
      }

    }
    else tolog('$rows_array = '.var_export($rows_array,1),0,false);
    file_put_contents($scenario_rowsfile,'rows_data = '.json_encode($rows_array));
    if(($errmsg = json_last_error()) != JSON_ERROR_NONE) {
      $show_errors[] = '<b style="color: darkred">$rows_array!!! ошибка кодирования в json: '.json_last_error_msg().'</b>';
    }
    else {
      return '<br />
           <b>Файл рядов для этого сайта подготовлен.</b> Всего товаров с рядами: '.$cnt.'<br />';
    }
  }

  function write_images() {
    global $images_log, $show_errors, $show_messages, $scenario_imagesfile;

    $scenario_imagesfile = isset($scenario_imagesfile)? $scenario_imagesfile : scenario_imagesfile;
    $error = false;
    tolog('Сбрасываем кеш товаров с картинками: '.count($images_log),0,0,0);
    if(count($images_log) >2000) {
      $chunks = array_chunk($images_log,1500,true);
      tolog('Количество частей: '.count($chunks),0,0,0);
      foreach ($chunks as $id=> $images_log_chunk) {
        $images_log_chunk['total'] = count($images_log);
        $images_log_chunk['parts'] = count($chunks);
        $images_log_chunk['part']  = ($id+1);
        $scenario_imagesfile_chunk = (!$id)?$scenario_imagesfile:str_replace('.up.json','-p'.($id+1).'.up.json',$scenario_imagesfile);
        tolog('$images_log_chunk №'.($id+1).' = '.@var_export($images_log_chunk,1),0,false);

        file_put_contents($scenario_imagesfile_chunk,(($id)?'images_data_part =  ':'images_data =  ').json_encode($images_log_chunk));
        if(($errmsg = json_last_error()) != JSON_ERROR_NONE) {
          $error = true;
          $show_errors[] = '<b style="color: darkred">$images_log!!! ошибка кодирования в json: '.json_last_error_msg().'</b>';
        }
      }
      if (!$error) return '<b>Файл картинок для этого сайта подготовлен. Всего товаров с картинками: '.count($images_log).', всего частей '.count($chunks).'</b>';

    }
    else {
      $images_log['total'] = count($images_log);
      $images_log['parts'] = 1;
      $images_log['part']  = 1;
      tolog('$images_log ('.count($images_log).') = '.var_export($images_log,1),0,false);

      file_put_contents($scenario_imagesfile,'images_data =  '.json_encode($images_log));
      if(($errmsg = json_last_error()) != JSON_ERROR_NONE) {
        $show_errors[] = '<b style="color: darkred">$images_log!!! ошибка кодирования в json: '.json_last_error_msg().'</b>';
      }
      else {
        return '<b>Файл картинок для этого сайта подготовлен. Всего товаров с картинками: '.(count($images_log)-3).'</b>';
      }
    }
  }

  function formatfilesize($path)
  {
      $size = filesize($path);
      $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
      $power = $size > 0 ? floor(log($size, 1024)) : 0;
      return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
  }

  function make_opts($opts) {
    if(!empty($opts) && !is_array($opts)) $opts = array($opts);
    $opts = array_unique(array_filter(array_map('trim',$opts)));
    if(!count($opts)) return '-';
    $opts = count($opts) > 1? implode('/',$opts) : array_shift($opts).'/';
    return $opts;
  }

  function array_json_decode(&$array) {
    foreach ($array as $site=>$users) foreach ($users as $uid => $ucats) foreach ($ucats as $catid => $data) {
      $array[$site][$uid][$catid] = json_decode($data,1);
    }
  }

  function check_validity(&$item_data) {
    global $show_errors;
    $item_data0 = $item_data;
    $item_data  = array_map('trim',$item_data);
    $datastring = '«'.implode('»; «',$item_data).'»';
    $cnt        = count($item_data);
    $item_data  = array_filter($item_data);
    if($cnt != count($item_data)) {
      $show_errors[] = '<span class="red">Данные товара недействительны. Товар пропущен.</span>: '.$datastring;
      tolog('Данные товара недействительны. Товар пропущен: '."\n".'$item_data0 = '.var_export($item_data0,1)."\n".' $item_data = '.var_export($item_data,1),0,0,0);
      return false;
    }
    return true;
  }

function register_error($str,$color='red',$lvl=4,$hidden_info=false) {
  global $show_errors;

  $show_errors[] = '<span style="color:'.$color.'">'.$str.'</span>';
  tolog('<span style="color:'.$color.'">'.$str.'</span>',$lvl);
  if($hidden_info) tolog($hidden_info,0,0,0);
  return false;
}

  function report_error($str,$color='red',$lvl=4,$hidden_info=false) {
    return register_error($str,$color,$lvl,$hidden_info);
  }


function clean_proxy_url($url) {
  return preg_replace('/&b=\d+$/ui','',str_replace('/p/browse.php?u=','',urldecode($url)));
}
