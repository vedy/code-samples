<?php
/*
* Справочник-шаблонизатор функций
* @vika.vedy
* 2012-2015
*/

/* -- Настройкии логина на сайт -- */
  function a0_login($postdata,$posturl='',$geturl=false,$encoding=null) {

    tolog('Входим на сайт '.SITE_URL.' с логином и паролем');
    $geturl = $geturl?$geturl:SITE_URL;
    phpQuery::newDocumentHTML(get_contents(make_full_url($geturl),0),$encoding);

    get_contents(make_full_url($posturl),0,0,$postdata,array('Referer'=>make_full_url($geturl)));
    if($geturl)
      phpQuery::newDocumentHTML(get_contents(make_full_url($geturl),0,0),$encoding);

  }

/* -- Настройкии логина на сайт со скрытыми полями -- */
  function a1_login_w_hidden($postdata,$posturl,$geturl=false,$encoding=null) {

    tolog('Входим на сайт '.SITE_URL.' с логином и паролем');
    $geturl = $geturl?$geturl:SITE_URL;
    phpQuery::newDocumentHTML(get_contents(make_full_url($geturl),0),$encoding);

    $hiddenf  = pq("input[type='hidden']");
    if(count($hiddenf)) foreach($hiddenf as $f) $postdata .= '&'.pq($f)->attr("name").'='.pq($f)->val();

    get_contents(make_full_url($posturl),0,0,$postdata,array('Referer'=>make_full_url($geturl)));
    if($geturl)
      phpQuery::newDocumentHTML(get_contents(make_full_url($geturl),0,0),$encoding);

  }
  
  function a2_check_login($url) {
    $pqstr = 'div.Opt a.Button';
    $checkstring = 'Выход';
    $postdata    = 'login=opt&pass=33333&user_act=login';
    $docId = false;
    $logged_in = stristr(pq($pqstr)->html(),$checkstring) !== false;
    if(!$logged_in) {
      $docId = phpQuery::newDocumentHTML(get_contents($url,0,false));
      $logged_in = pq($pqstr)->html() == $checkstring;
    }
    if(!$logged_in) {
      tolog('<span style="color: green">Входим на сайт с логином и паролем…</span>');
      $content = get_contents($url,0,true,$postdata,array('Referer'=>$url));
      $docId   = phpQuery::newDocumentHTML($content);
      $logged_in = stristr(pq($pqstr)->html(),$checkstring) !== false;
    }
    if(!$logged_in) {
      tolog('<font color=red>Произошла <u>деавторизация</u> при запросе содержимого страницы <a href="'.$url.'">'.$url.'</a>. Повторная авторизация не удалась. Если эта ошибка повторяется слишком часто, попробуйте остановить процесс и повторить через несколько часов.</font>',1);
      remove_contents($url);
      tolog('<b style="color: red; font-size: 120%"Окончен парсинг сайта '.SITE_URL.'</b>');
      unlink(scenario_statusfile);
      return false;

    }
    return $docId;
      
  }

/* -- Стандартное начало парсинга: -- */
  function b0_start_parsing($bookmark=false,$custom_action = false) {
    global $scenario_site, $categories, $items_array;
    //$bookmark = '"-";"Закладка";"0";"Положите эту позицию в корзинку, чтобы не потерять закупку";"";""';

    if(isset($_REQUEST['startparsing'])) {
      $_SESSION[scenario_name.'totalitems'] = 0;
      $_SESSION[scenario_name.'totalitemsparsed'] = 0;
      $_SESSION[scenario_name.'finished'] = array();
      $_SESSION[scenario_name.'percent'] = 0;

      tolog('<b>Парсим сайт: '.scenario_website.'</b>');
      tolog('$_COOKIE = '.var_export($_COOKIE,1),0,0,0);

      if(!$bookmark && file_exists(scenario_updatefile)) unlink (scenario_updatefile);
      else if($bookmark) file_put_contents(scenario_updatefile, mb_convert_encoding($bookmark,'Windows-1251','UTF8')."\r\n");
      if($custom_action) call_user_func($custom_action);
      if(empty($_REQUEST['cookies']) || !isset($_REQUEST['cookies'])) unset($_SESSION['set_cookies']);
      elseif(!empty($_SESSION['set_cookies']))
        tolog('<h4>Работаем с куками: '.$_SESSION['set_cookies'].'</h4>');
      require_once (dirname(__FILE__).'/phpQuery-onefile.php');
      $categories  = get_cats();
      if(count($categories))
        $items_array = get_items($categories);
    }
  }


/* -- Стандартная проверка на возможность выбора списка категорий -- */
  function b1_check_catmatches(&$cat_matches) {
    global $scenario_site;
    if(!$cat_matches || !count($cat_matches)) {
          tolog('<b style="color: red; font-size: 120%">Не найдено ни одной категории. Возможно, изменилась структура сайта-донора или сайт не работает<hr />Окончен парсинг сайта '.$scenario_site.'</b>');
          unlink(scenario_statusfile);
          return array();
    }
  }

/* -- Сохраняем ряды в массив -- */
  function b2_set_rows() {
    global $item_rows,$rows_array,$item_data,$rows_array;
    if(count($item_rows)) {
      $rows_array[trim(mb_substr($item_data['sku'],0,20,'UTF-8')).': '.$item_data['name']] = populate_rows($item_rows);
      $item_data['rows'] = 'Ряды: '.$rows_array[trim(mb_substr($item_data['sku'],0,20,'UTF-8')).': '.$item_data['name']];
    }
  }

  function d0_decode_iso($str) {
    return mb_convert_encoding(mb_convert_encoding(mb_convert_encoding($str, 'ISO-8859-1', 'UTF-8'), 'UTF-8', "Windows-1251"), 'UTF-8', 'UTF-8');
  }


/* -- Стандартный запрос csv-фалй по частям: -- */
  function f0_get_gsv_file_part () {
    global $scenario_resultdir;

    if (isset($_REQUEST['getcsv'])  && file_exists($scenario_resultdir.urldecode($_REQUEST['getcsv'])) ) {

      $filename = empty($_REQUEST['getcsv'])? basename(scenario_updatefile) : urldecode($_REQUEST['getcsv']);
      $file = $scenario_resultdir.$filename;
      $show_name = scenario_name.date('-Ymd',filemtime($file)).'-update.csv';

        header('Content-Description: File Transfer');
        header('Content-Type: text/csv; charset=Windows-1251',1);
      if(isset($_REQUEST['p']) && is_numeric($_REQUEST['p']))
        header('Content-Disposition: attachment; filename="'.str_replace('.csv','-part'.($_REQUEST['p']+1).'.csv',$show_name).'"');
      else
        header('Content-Disposition: attachment; filename="'.$show_name.'"');

        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
      if(isset($_REQUEST['p']) && is_numeric($_REQUEST['p'])) {
        $slicesize = (isset($_GET['s']) && is_numeric($_GET['s']))? intval($_GET['s']) : 500;
        $ca = file($file,FILE_SKIP_EMPTY_LINES);
        $contents = ($_REQUEST['p'] > ceil(count($ca)/100) || (int)$_REQUEST['p'] < 0)?
                      file_get_contents($file):
                      implode("",array_slice($ca,$_REQUEST['p']*$slicesize,$slicesize));
        header('Content-Length: ' . strlen($contents));
        ob_clean(); flush();
        exit($contents);
      }
      else {
        header('Content-Length: ' . filesize($file));
        ob_clean(); flush();
        readfile($file);
        exit;
      }
    }
  }


/* -- Выборка последней ссылки на страницы: -- */
  function c0_last_page_link_finding () {
    // способ 1
    $last_page_link_obj = pq("a[class^='pageLink']")->eq(-1);

    // способ 2
    $lastpage = pq("div.wpsc_page_numbers_bottom a[title='Последняя страница']:first")->attr('href');
  }

/* --Удаление мешающего элемента -- */
  function c1_remove_element() {
    pq("div.pp_top h1")->remove();
  }

//загрузка прайса в формате CSV
  function  c4_csv_load() {
    global $scenario_site,$items_array,$price_data;

    if(!isset($_FILES["opts_excel_file"]) && !isset($_SESSION['load_file'][scenario_name])) {
      tolog('<b style="color: red; font-size: 120%">Прайс-лист не был загружен. Выберите на своём компьютере свежий прайс-лист в формате CSV (< 10Мб)<br />Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return false;

    }

    $price_data = 0;
    $price_data_par = array();
    $uploaded_files = get_file("opts_excel_file");

    tolog(__LINE__.": \$uploaded_files = ".var_export($uploaded_files,true),0,false);
    if(is_array($uploaded_files)) {
      foreach ($uploaded_files as $id=>$m) if(is_array($m)) {
        tolog('<span style="color: blue">'.__LINE__.': Идёт обработка прайса «'.$m["name"].'». Ждите…</span>');
        $price_data_part  = iterate_csv_file ($m['path'],$m['name']);
        tolog($m['path'].' $price_data_part rows = '.count($price_data_part),0,0,0);
        $price_data  +=count($price_data_part);


        if(!$price_data_part || !is_array($price_data_part) || !count($price_data_part)) {
          tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый прайс-лист ('.$m["name"].')</b>');
          continue;
        }

        if(isset($_COOKIE['debug'])) tolog('debug: $price_data = '.var_export($price_data_part,1),0,0,0);
        $GLOBALS['pricename'] = $m["name"];
        iterate_price_data ($price_data_part);
        unset($price_data_part);
      }
    }
    else if (isset($_SESSION['load_file'][scenario_name]) && is_array($_SESSION['load_file'][scenario_name]) ) {
      tolog(__LINE__.": \$_SESSION['load_file'][scenario_name] = ".var_export($_SESSION['load_file'][scenario_name],true),0,false);
      foreach ($_SESSION['load_file'][scenario_name] as $m) if(is_array($m)) {
        tolog('<span style="color: blue">'.__LINE__.': Идёт обработка прайса «'.$m["name"].'». Ждите…</span>');
        $price_data_part  = iterate_csv_file ($m['path'],$m['name']);
        tolog($m['path'].' $price_data_part rows = '.count($price_data_part),0,0,0);
        $price_data  +=count($price_data_part);


        if(!$price_data_part || !is_array($price_data_part) || !count($price_data_part)) {
          tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый прайс-лист ('.$m["name"].')</b>');
          continue;
        }

        if(isset($_COOKIE['debug'])) tolog('debug: $price_data = '.var_export($price_data_part,1),0,0,0);
        $GLOBALS['pricename'] = $m["name"];
        iterate_price_data ($price_data_part);
        unset($price_data_part);
      }
    }

    if($price_data==0) {
      tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый прайс-лист ('.@$_SESSION['load_file'][scenario_name]['name'].').<br />Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return false;
    }
    unset($price_data);
  }

//загрузка и обработка прайса
  function  c3_price_load($calculate=false) {
    global $scenario_site,$items_array,$price_data;

    if(!isset($_FILES["opts_excel_file"]) && !isset($_SESSION['load_file'][scenario_name])) {
      tolog('<b style="color: red; font-size: 120%">Прайс-лист не был загружен. Выберите на своём компьютере свежий прайс-лист в указанном формате<br />Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return false;

    }

    if(isset($_FILES["opts_excel_file"]) && ($m = get_file("opts_excel_file") )!== null) {
      tolog('<span style="color: blue">'.__LINE__.': Идёт обработка прайса «'.$m["name"].'». Ждите…</span>');
      $price_data  = iterate_file ($m['path'],$m['name'],$calculate);
    }
    else if (isset($_SESSION['load_file'][scenario_name]) && is_array($_SESSION['load_file'][scenario_name]) ) {
      $m = $_SESSION['load_file'][scenario_name];
      tolog('<span style="color: blue">'.__LINE__.': Идёт обработка прайса «'.$m["name"].'». Ждите…</span>');
      $price_data  = iterate_file ( $m['path'], $m['name'],$calculate);
    }


    if(!$price_data || !is_array($price_data) || !count($price_data)) {
      tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый прайс-лист ('.@$_SESSION['load_file'][scenario_name]['name'].').<br />Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return false;
    }

    //tolog("\$price_data = ".var_export($price_data,true),0,false);
    $GLOBALS['pricename'] = $m["name"];
    iterate_price_data ($price_data);
    unset($price_data);

  }


  function  c2_export_load_array($calculate=false,$required=true) {
    global $scenario_site,$items_array,$price_data;



    if(!isset($_FILES["opts_excel_file"])) {
      if($required) {
        tolog('<b style="color: red; font-size: 120%">Экспортный файл не был загружен. Нажмите в каталоге кнопку «Экспорт», сохраните получившийся файл и и загрузите в программу этот файл (report_'.date('Y-m-d').'.xls)<br />Окончен парсинг сайта '.$scenario_site.'</b>');
        unlink(scenario_statusfile);
      }

      return false;

    }

    $price_data = 0;
    $price_data_part = array();
    $uploaded_files = [];
    if (isset($_FILES["opts_excel_file"])) {
      $uploaded_files = get_file("opts_excel_file");
      tolog(__LINE__.": \$uploaded_files = ".var_export($uploaded_files,true),0,false);
    }
    if(is_array($uploaded_files) and $uploaded_files) {
      foreach ($uploaded_files as $id=>$m) if(is_array($m)) {
        tolog('<span style="color: blue">'.__LINE__.': Идёт обработка файла «'.$m["name"].'». Ждите…</span>');
        $price_data_part  = iterate_html_file ($m['path'],$m['name']);
        tolog($m['path'].' $price_data_part rows = '.count($price_data_part),0,0,0);
        $price_data  +=count($price_data_part);


        if(!$price_data_part || !is_array($price_data_part) || !count($price_data_part)) {
          tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый файл ('.$m["name"].')</b>');
          continue;
        }

        if(isset($_COOKIE['debug'])) tolog('debug: $price_data = '.var_export($price_data_part,1),0,0,0);
        $GLOBALS['pricename'] = $m["name"];
        iterate_price_data ($price_data_part);
        unset($price_data_part);
      }
    }


    if($price_data == 0) {
      tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженые файлы<br />Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return false;
    }

    return true;

  }


  function  c2_price_load_array($calculate=false,$required=true) {
    global $scenario_site,$items_array,$price_data;

    if(!isset($_FILES["opts_excel_file"]) && !isset($_SESSION['load_file'][scenario_name])) {
      if($required) {
        tolog('<b style="color: red; font-size: 120%">Прайс-лист не был загружен. Выберите на своём компьютере свежий прайс-лист в указанном формате.<br />Окончен парсинг сайта '.$scenario_site.'</b>');
        unlink(scenario_statusfile);
      }

      return false;

    }

    $price_data = 0;
    $price_data_part = array();
    $uploaded_files = [];
    if (isset($_FILES["opts_excel_file"])) {
      $uploaded_files = get_file("opts_excel_file");
      tolog(__LINE__.": \$uploaded_files = ".var_export($uploaded_files,true),0,false);
    }
    if(is_array($uploaded_files) and $uploaded_files) {
      foreach ($uploaded_files as $id=>$m) if(is_array($m)) {
        tolog('<span style="color: blue">'.__LINE__.': Идёт обработка прайса «'.$m["name"].'». Ждите…</span>');
        $price_data_part  = iterate_file ($m['path'],$m['name'],$calculate);
        tolog($m['path'].' $price_data_part rows = '.count($price_data_part),0,0,0);
        $price_data  +=count($price_data_part);


        if(!$price_data_part || !is_array($price_data_part) || !count($price_data_part)) {
          tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый прайс-лист ('.$m["name"].')</b>');
          continue;
        }

        if(isset($_COOKIE['debug'])) tolog('debug: $price_data = '.var_export($price_data_part,1),0,0,0);
        $GLOBALS['pricename'] = $m["name"];
        iterate_price_data ($price_data_part);
        unset($price_data_part);
      }
    }
    else if (isset($_SESSION['load_file'][scenario_name]) && is_array($_SESSION['load_file'][scenario_name]) ) {
      tolog(__LINE__.": \$_SESSION['load_file'][scenario_name] = ".var_export($_SESSION['load_file'][scenario_name],true),0,false);
      if (isset($_SESSION['load_file'][scenario_name][0]) and 
              ! is_array($_SESSION['load_file'][scenario_name][0])) {
          $_SESSION['load_file'][scenario_name] = [$_SESSION['load_file'][scenario_name]]; 
      }
      foreach ($_SESSION['load_file'][scenario_name] as $m) {
        tolog('<span style="color: blue">'.__LINE__.': Идёт обработка прайса «'.$m["name"].'». Ждите…</span>');
        $price_data_part  = iterate_file ($m['path'],$m['name'],$calculate);
        tolog($m['path'].' $price_data_part rows = '.count($price_data_part),0,0,0);
        $price_data  +=count($price_data_part);


        if(!$price_data_part || !is_array($price_data_part) || !count($price_data_part)) {
          tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженый прайс-лист ('.$m["name"].')</b>');
          continue;
        }

        if(isset($_COOKIE['debug'])) tolog('debug: $price_data = '.var_export($price_data_part,1),0,0,0);
        $GLOBALS['pricename'] = $m["name"];
        iterate_price_data ($price_data_part);
        unset($price_data_part);
      }
    }


    if($price_data == 0) {
      tolog('<b style="color: red; font-size: 120%">Не удалось обработать загруженые прайс-листы<br />Окончен парсинг сайта '.$scenario_site.'</b>');
      unlink(scenario_statusfile);
      return false;
    }

    return true;

  }


/* -- Рекурсивный обход страниц в «бесконечном» пейджере (с нахождением карточек товаров) -- */
  function d0_recursive_continuously_pager_with_utems_cards () {
    global $pages_links, $cat_name;
    while (list($key, $plink) = each($pages_links)) {
      tolog($plink,4);
      phpQuery::newDocumentHTML(get_contents($plink,cacheTime));
      $newplinks = pq("a[href*='page=']");
  //    $itemlinks = pq('p.unit_img a');
  //    foreach ($itemlinks as $itemlink) {
  //      $url = make_full_url(pq($itemlink)->attr('href'));
  //      if(!in_array($url,$check_items)) {
  //        $cats_array[$cat_name]['items'][] = array(
  //          'url'=> $url,
  //          'img'=> pq('img[src^="/catalog/image/"]',$itemlink)->attr("src"));
  //        $check_items[] = $url;
  //      }
  //    }
      $new = 0;
      foreach ($newplinks as $newplink) {
        $link = make_full_url(pq($newplink)->attr('href'));
        if(!in_array($link,$pages_links)) {
          $pages_links[] = $link;
          $cats_array[$cat_name]['links'][] = $link;
          $new++;
        }
      }
    }
  }

//записываем товар в файл
  function d9_put2csv ($item_to_cat,$file=false,$convert = false) {

    if(!$file) $file = scenario_updatefile;
//    else tolog('$file = '.$file);

    if($convert) $item_to_cat = to_cp1251($item_to_cat);
    if (!file_put_contents($file, $item_to_cat."\r\n",FILE_APPEND)) {
      tolog('<font color=red>Произошла ошибка при записи в файл '.$file.'  строки: «'.$item_to_cat.'»</font>',1);
      logerror('Произошла ошибка при сохранении в .csv-файл "'.$file.'" строки: «'.$item_to_cat.'»'."\r\n");
      return;
    }
    $_SESSION[scenario_name.'percent'] = round($_SESSION[scenario_name.'totalitemsparsed']*100/$_SESSION[scenario_name.'totalitems']);
    $_SESSION[scenario_name.'totalitemsparsed'] += 1;
    file_put_contents(scenario_statusfile,time());
  }



/* проверка на перегрузку */
//  function get_contents_overload($url,$days=2,$timeout=true) {
//     $content = get_contents($cur_url,$days,$timeout);
//     if(preg_match('/Temporary Access Denial/',$content)) {
//       remove_contents($url);
//       tolog('<span style="color: red">Сайт поставщика перегружен запросами. Сделаем перерывчик минут 5.</span>');
//       sleep(300);
//       $content = get_contents($cur_url,$days,$timeout);
//       if(preg_match('/Temporary Access Denial/',$content)) {
//         remove_contents($url);
//         tolog('<span style="color: red">Перерывчик не помог. Попробуйте запустить обработку ещё раз через полчасика.</span><br>Обработка остановлена');
//         unlink(scenario_statusfile);
//         return false;
//       }
//     }
//     return $content;
//   }


/* -- Логин и проверка всех страниц --*/
  function check_login($url,$from_cache = false) {
    $pqstr       = 'a[href="/account"]';
    $checkstring = 'Мой аккаунт';
    $postdata    = 'user%5Bemail%5D=oppa%40mail.ru&user%5Bpassword%5D=12345678&commit=%D0%92%D0%BE%D0%B9%D1%82%D0%B8';
    $docId       = false;
    $logged_in   = pq($pqstr)->eq(0)->html() == $checkstring;

    if(!$logged_in && $from_cache) {
      $docId = phpQuery::newDocumentHTML(get_contents($url,0,true));
      $logged_in = pq($pqstr)->eq(0)->html() == $checkstring;
    }
    if(!$logged_in) {
      tolog('<span style="color: green">Входим на сайт с логином и паролем…</span>');
//      $hiddenf  = pq("form#new_user input[type='hidden']");
//      if(count($hiddenf)) foreach($hiddenf as $f) $postdata .= '&'.pq($f)->attr("name").'='.pq($f)->val();

      get_contents(SITE_URL.'/account/sign_in',0,true,$postdata,array('Referer'=>$url));
      $content = get_contents($url,0,true);
      $docId   = phpQuery::newDocumentHTML($content);
      $logged_in = pq($pqstr)->eq(0)->html() == $checkstring;
    }
    if(!$logged_in) {
      tolog('<font color=red>Произошла <u>деавторизация</u> при запросе содержимого страницы <a href="'.$url.'">'.$url.'</a>. Повторная авторизация не удалась. Если эта ошибка повторяется слишком часто, попробуйте остановить процесс и повторить через несколько часов.</font>',1);
      $check_html = pq($pqstr)->eq(0)->html();
      tolog('html = '.$check_html,0,0,0);
//      if(!empty($check_html)) remove_contents($url);
      tolog('<b style="color: red; font-size: 120%">Окончен парсинг сайта '.SITE_URL.'</b>');
      unlink(scenario_statusfile);
      return false;

    }
    return $docId;

  }

  // Настройкии логина на сайт со скрытыми полями
  function login_w_hidden($postdata,$posturl,$geturl=false,$encoding=null) {

    tolog('Входим на сайт '.SITE_URL.' с логином и паролем');
    $geturl = $geturl?$geturl:SITE_URL;

    get_contents(make_full_url($posturl),0,0,$postdata,array('Referer'=>make_full_url($geturl)));
    if($geturl)
      phpQuery::newDocumentHTML(get_contents(make_full_url($geturl),0,0),$encoding);

  }

